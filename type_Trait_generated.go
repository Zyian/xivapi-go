// Code generated by generate_structs - DO NOT EDIT.

package xivapi

type Trait struct {
	Name_en string `json:"Name_en,omitempty"`
	Value int `json:"Value,omitempty"`
	ClassJobCategoryTarget string `json:"ClassJobCategoryTarget,omitempty"`
	ClassJobCategoryTargetID int `json:"ClassJobCategoryTargetID,omitempty"`
	DescriptionJSON_kr interface{} `json:"DescriptionJSON_kr,omitempty"`
	Description_en string `json:"Description_en,omitempty"`
	Icon string `json:"Icon,omitempty"`
	IconID int `json:"IconID,omitempty"`
	Quest interface{} `json:"Quest,omitempty"`
	ClassJobCategory interface{} `json:"ClassJobCategory,omitempty"`
	DescriptionJSON_de []interface{} `json:"DescriptionJSON_de,omitempty"`
	DescriptionJSON_fr []interface{} `json:"DescriptionJSON_fr,omitempty"`
	Description_fr string `json:"Description_fr,omitempty"`
	ID int `json:"ID,omitempty"`
	Name_de string `json:"Name_de,omitempty"`
	DescriptionJSON []interface{} `json:"DescriptionJSON,omitempty"`
	DescriptionJSON_ja []interface{} `json:"DescriptionJSON_ja,omitempty"`
	Name string `json:"Name,omitempty"`
	Url string `json:"Url,omitempty"`
	ClassJob interface{} `json:"ClassJob,omitempty"`
	DescriptionJSON_en []interface{} `json:"DescriptionJSON_en,omitempty"`
	Description_de string `json:"Description_de,omitempty"`
	Description_kr interface{} `json:"Description_kr,omitempty"`
	ClassJobTarget string `json:"ClassJobTarget,omitempty"`
	ClassJobTargetID int `json:"ClassJobTargetID,omitempty"`
	Description string `json:"Description,omitempty"`
	Description_cn interface{} `json:"Description_cn,omitempty"`
	GameContentLinks []interface{} `json:"GameContentLinks,omitempty"`
	Name_fr string `json:"Name_fr,omitempty"`
	Description_ja string `json:"Description_ja,omitempty"`
	Name_ja string `json:"Name_ja,omitempty"`
	QuestTarget string `json:"QuestTarget,omitempty"`
	DescriptionJSON_cn interface{} `json:"DescriptionJSON_cn,omitempty"`
	Level int `json:"Level,omitempty"`
	QuestTargetID int `json:"QuestTargetID,omitempty"`
}
