// Code generated by generate_structs - DO NOT EDIT.

package xivapi

type GatheringPointBonus struct {
	Condition interface{} `json:"Condition,omitempty"`
	ConditionTarget string `json:"ConditionTarget,omitempty"`
	ConditionTargetID int `json:"ConditionTargetID,omitempty"`
	Url string `json:"Url,omitempty"`
	BonusType interface{} `json:"BonusType,omitempty"`
	BonusTypeTarget string `json:"BonusTypeTarget,omitempty"`
	BonusTypeTargetID int `json:"BonusTypeTargetID,omitempty"`
	ID int `json:"ID,omitempty"`
	BonusValue int `json:"BonusValue,omitempty"`
	ConditionValue int `json:"ConditionValue,omitempty"`
	GameContentLinks []interface{} `json:"GameContentLinks,omitempty"`
}
