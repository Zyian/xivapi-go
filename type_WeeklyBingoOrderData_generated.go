// Code generated by generate_structs - DO NOT EDIT.

package xivapi

type WeeklyBingoOrderData struct {
	Type int `json:"Type,omitempty"`
	Url string `json:"Url,omitempty"`
	GameContentLinks []interface{} `json:"GameContentLinks,omitempty"`
	ID int `json:"ID,omitempty"`
	Text interface{} `json:"Text,omitempty"`
	TextTargetID int `json:"TextTargetID,omitempty"`
	Data int `json:"Data,omitempty"`
	Icon string `json:"Icon,omitempty"`
	IconID int `json:"IconID,omitempty"`
	TextTarget string `json:"TextTarget,omitempty"`
}
