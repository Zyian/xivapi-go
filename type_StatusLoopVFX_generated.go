// Code generated by generate_structs - DO NOT EDIT.

package xivapi

type StatusLoopVFX struct {
	VFX struct {
		ID int `json:"ID,omitempty"`
		Location string `json:"Location,omitempty"`
		Location_en string `json:"Location_en,omitempty"`
	}
	VFX2Target string `json:"VFX2Target,omitempty"`
	VFX2TargetID int `json:"VFX2TargetID,omitempty"`
	VFX3 interface{} `json:"VFX3,omitempty"`
	VFX3TargetID int `json:"VFX3TargetID,omitempty"`
	GameContentLinks struct {
		Status struct {
			VFX []interface{} `json:"VFX,omitempty"`
		}
	}
	Url string `json:"Url,omitempty"`
	VFX3Target string `json:"VFX3Target,omitempty"`
	VFXTarget string `json:"VFXTarget,omitempty"`
	VFXTargetID int `json:"VFXTargetID,omitempty"`
	ID int `json:"ID,omitempty"`
	VFX2 interface{} `json:"VFX2,omitempty"`
}
