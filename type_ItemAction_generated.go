// Code generated by generate_structs - DO NOT EDIT.

package xivapi

type ItemAction struct {
	Data2 int `json:"Data2,omitempty"`
	Data3 int `json:"Data3,omitempty"`
	Data7 int `json:"Data7,omitempty"`
	DataHQ1 int `json:"DataHQ1,omitempty"`
	DataHQ2 int `json:"DataHQ2,omitempty"`
	DataHQ3 int `json:"DataHQ3,omitempty"`
	DataHQ4 int `json:"DataHQ4,omitempty"`
	DataHQ5 int `json:"DataHQ5,omitempty"`
	GameContentLinks struct {
		Item struct {
			ItemAction []interface{} `json:"ItemAction,omitempty"`
		}
	}
	Data5 int `json:"Data5,omitempty"`
	Data6 int `json:"Data6,omitempty"`
	Data0 int `json:"Data0,omitempty"`
	Data1 int `json:"Data1,omitempty"`
	Data4 int `json:"Data4,omitempty"`
	Data8 int `json:"Data8,omitempty"`
	DataHQ0 int `json:"DataHQ0,omitempty"`
	DataHQ6 int `json:"DataHQ6,omitempty"`
	Type int `json:"Type,omitempty"`
	DataHQ7 int `json:"DataHQ7,omitempty"`
	DataHQ8 int `json:"DataHQ8,omitempty"`
	ID int `json:"ID,omitempty"`
	Url string `json:"Url,omitempty"`
}
