// Code generated by generate_structs - DO NOT EDIT.

package xivapi

type BuddyAction struct {
	Description string `json:"Description,omitempty"`
	IconStatus string `json:"IconStatus,omitempty"`
	Url string `json:"Url,omitempty"`
	IconStatusID int `json:"IconStatusID,omitempty"`
	Description_en string `json:"Description_en,omitempty"`
	Icon string `json:"Icon,omitempty"`
	Name_de string `json:"Name_de,omitempty"`
	Name_en string `json:"Name_en,omitempty"`
	Name_ja string `json:"Name_ja,omitempty"`
	Name_fr string `json:"Name_fr,omitempty"`
	Description_de string `json:"Description_de,omitempty"`
	Description_fr string `json:"Description_fr,omitempty"`
	Description_ja string `json:"Description_ja,omitempty"`
	GameContentLinks []interface{} `json:"GameContentLinks,omitempty"`
	ID int `json:"ID,omitempty"`
	IconID int `json:"IconID,omitempty"`
	Name string `json:"Name,omitempty"`
}
