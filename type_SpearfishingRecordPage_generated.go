// Code generated by generate_structs - DO NOT EDIT.

package xivapi

type SpearfishingRecordPage struct {
	PlaceName struct {
		Name_en string `json:"Name_en,omitempty"`
		ID int `json:"ID,omitempty"`
		Name string `json:"Name,omitempty"`
		NameNoArticle_de string `json:"NameNoArticle_de,omitempty"`
		NameNoArticle_fr string `json:"NameNoArticle_fr,omitempty"`
		Name_de string `json:"Name_de,omitempty"`
		Name_fr string `json:"Name_fr,omitempty"`
		Name_ja string `json:"Name_ja,omitempty"`
		Icon string `json:"Icon,omitempty"`
		NameNoArticle string `json:"NameNoArticle,omitempty"`
		NameNoArticle_en string `json:"NameNoArticle_en,omitempty"`
		NameNoArticle_ja string `json:"NameNoArticle_ja,omitempty"`
	}
	PlaceNameTarget string `json:"PlaceNameTarget,omitempty"`
	PlaceNameTargetID int `json:"PlaceNameTargetID,omitempty"`
	Url string `json:"Url,omitempty"`
	GameContentLinks []interface{} `json:"GameContentLinks,omitempty"`
	ID int `json:"ID,omitempty"`
	Image string `json:"Image,omitempty"`
	ImageID int `json:"ImageID,omitempty"`
}
