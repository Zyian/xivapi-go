// Code generated by generate_structs - DO NOT EDIT.

package xivapi

type SubmarinePart struct {
	RepairMaterials int `json:"RepairMaterials,omitempty"`
	Speed int `json:"Speed,omitempty"`
	Favor int `json:"Favor,omitempty"`
	Range int `json:"Range,omitempty"`
	Rank int `json:"Rank,omitempty"`
	Retrieval int `json:"Retrieval,omitempty"`
	Slot int `json:"Slot,omitempty"`
	Surveillance int `json:"Surveillance,omitempty"`
	Url string `json:"Url,omitempty"`
	Components int `json:"Components,omitempty"`
	GameContentLinks []interface{} `json:"GameContentLinks,omitempty"`
	ID int `json:"ID,omitempty"`
}
