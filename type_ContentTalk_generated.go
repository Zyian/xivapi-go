// Code generated by generate_structs - DO NOT EDIT.

package xivapi

type ContentTalk struct {
	Text_fr string `json:"Text_fr,omitempty"`
	ContentTalkParamTarget string `json:"ContentTalkParamTarget,omitempty"`
	GameContentLinks struct {
		ContentNpcTalk struct {
			ContentTalk1 []interface{} `json:"ContentTalk1,omitempty"`
		}
	}
	ID int `json:"ID,omitempty"`
	Text string `json:"Text,omitempty"`
	Text_de string `json:"Text_de,omitempty"`
	ContentTalkParam struct {
		ID int `json:"ID,omitempty"`
		Param int `json:"Param,omitempty"`
		TestAction interface{} `json:"TestAction,omitempty"`
		TestActionTarget string `json:"TestActionTarget,omitempty"`
		TestActionTargetID int `json:"TestActionTargetID,omitempty"`
	}
	ContentTalkParamTargetID int `json:"ContentTalkParamTargetID,omitempty"`
	Text_en string `json:"Text_en,omitempty"`
	Text_ja string `json:"Text_ja,omitempty"`
	Url string `json:"Url,omitempty"`
}
