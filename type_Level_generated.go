// Code generated by generate_structs - DO NOT EDIT.

package xivapi

type Level struct {
	MapTarget string `json:"MapTarget,omitempty"`
	MapTargetID int `json:"MapTargetID,omitempty"`
	Object int `json:"Object,omitempty"`
	Type int `json:"Type,omitempty"`
	Url string `json:"Url,omitempty"`
	X int `json:"X,omitempty"`
	EventId int `json:"EventId,omitempty"`
	Radius int `json:"Radius,omitempty"`
	TerritoryTarget string `json:"TerritoryTarget,omitempty"`
	Y int `json:"Y,omitempty"`
	Yaw int `json:"Yaw,omitempty"`
	GameContentLinks []interface{} `json:"GameContentLinks,omitempty"`
	Z int `json:"Z,omitempty"`
	ID int `json:"ID,omitempty"`
	Territory struct {
		ArrayEventHandlerTarget string `json:"ArrayEventHandlerTarget,omitempty"`
		Bg_en string `json:"Bg_en,omitempty"`
		Map struct {
			TerritoryTypeTarget string `json:"TerritoryTypeTarget,omitempty"`
			DiscoveryArrayByte int `json:"DiscoveryArrayByte,omitempty"`
			MapFilename string `json:"MapFilename,omitempty"`
			OffsetY int `json:"OffsetY,omitempty"`
			PlaceName interface{} `json:"PlaceName,omitempty"`
			PlaceNameRegionTarget string `json:"PlaceNameRegionTarget,omitempty"`
			SizeFactor int `json:"SizeFactor,omitempty"`
			PlaceNameSubTargetID int `json:"PlaceNameSubTargetID,omitempty"`
			TerritoryTypeTargetID int `json:"TerritoryTypeTargetID,omitempty"`
			PlaceNameSubTarget string `json:"PlaceNameSubTarget,omitempty"`
			PlaceNameTarget string `json:"PlaceNameTarget,omitempty"`
			Hierarchy int `json:"Hierarchy,omitempty"`
			ID int `json:"ID,omitempty"`
			MapMarkerRange int `json:"MapMarkerRange,omitempty"`
			PlaceNameRegion interface{} `json:"PlaceNameRegion,omitempty"`
			PlaceNameRegionTargetID int `json:"PlaceNameRegionTargetID,omitempty"`
			PlaceNameSub interface{} `json:"PlaceNameSub,omitempty"`
			DiscoveryIndex int `json:"DiscoveryIndex,omitempty"`
			MapFilenameId string `json:"MapFilenameId,omitempty"`
			OffsetX int `json:"OffsetX,omitempty"`
			PlaceNameTargetID int `json:"PlaceNameTargetID,omitempty"`
			TerritoryType interface{} `json:"TerritoryType,omitempty"`
		}
		MapTarget string `json:"MapTarget,omitempty"`
		MapTargetID int `json:"MapTargetID,omitempty"`
		PlaceName struct {
			NameNoArticle_en string `json:"NameNoArticle_en,omitempty"`
			NameNoArticle_fr string `json:"NameNoArticle_fr,omitempty"`
			NameNoArticle_ja string `json:"NameNoArticle_ja,omitempty"`
			Name string `json:"Name,omitempty"`
			Icon string `json:"Icon,omitempty"`
			NameNoArticle string `json:"NameNoArticle,omitempty"`
			NameNoArticle_de string `json:"NameNoArticle_de,omitempty"`
			Name_de string `json:"Name_de,omitempty"`
			Name_en string `json:"Name_en,omitempty"`
			Name_fr string `json:"Name_fr,omitempty"`
			Name_ja string `json:"Name_ja,omitempty"`
			ID int `json:"ID,omitempty"`
		}
		PlaceNameTarget string `json:"PlaceNameTarget,omitempty"`
		AetheryteTarget string `json:"AetheryteTarget,omitempty"`
		PlaceNameZoneTarget string `json:"PlaceNameZoneTarget,omitempty"`
		TerritoryIntendedUse int `json:"TerritoryIntendedUse,omitempty"`
		PlaceNameTargetID int `json:"PlaceNameTargetID,omitempty"`
		ID int `json:"ID,omitempty"`
		PlaceNameRegionTarget string `json:"PlaceNameRegionTarget,omitempty"`
		PlaceNameRegionTargetID int `json:"PlaceNameRegionTargetID,omitempty"`
		PlaceNameZone struct {
			Name string `json:"Name,omitempty"`
			NameNoArticle_de string `json:"NameNoArticle_de,omitempty"`
			NameNoArticle_fr string `json:"NameNoArticle_fr,omitempty"`
			NameNoArticle_ja string `json:"NameNoArticle_ja,omitempty"`
			Name_fr string `json:"Name_fr,omitempty"`
			Name_ja string `json:"Name_ja,omitempty"`
			ID int `json:"ID,omitempty"`
			Icon string `json:"Icon,omitempty"`
			NameNoArticle string `json:"NameNoArticle,omitempty"`
			NameNoArticle_en string `json:"NameNoArticle_en,omitempty"`
			Name_de string `json:"Name_de,omitempty"`
			Name_en string `json:"Name_en,omitempty"`
		}
		AetheryteTargetID int `json:"AetheryteTargetID,omitempty"`
		ArrayEventHandler struct {
			Data1 int `json:"Data1,omitempty"`
			Data13 int `json:"Data13,omitempty"`
			Data9 int `json:"Data9,omitempty"`
			Data0 int `json:"Data0,omitempty"`
			Data14 int `json:"Data14,omitempty"`
			Data2 int `json:"Data2,omitempty"`
			Data6 int `json:"Data6,omitempty"`
			Data10 int `json:"Data10,omitempty"`
			Data11 int `json:"Data11,omitempty"`
			Data12 int `json:"Data12,omitempty"`
			Data3 int `json:"Data3,omitempty"`
			Data5 int `json:"Data5,omitempty"`
			Data15 int `json:"Data15,omitempty"`
			Data4 int `json:"Data4,omitempty"`
			Data7 int `json:"Data7,omitempty"`
			Data8 int `json:"Data8,omitempty"`
			ID int `json:"ID,omitempty"`
		}
		ArrayEventHandlerTargetID int `json:"ArrayEventHandlerTargetID,omitempty"`
		Bg string `json:"Bg,omitempty"`
		Name string `json:"Name,omitempty"`
		Name_en string `json:"Name_en,omitempty"`
		PlaceNameZoneTargetID int `json:"PlaceNameZoneTargetID,omitempty"`
		Aetheryte struct {
			PlaceName interface{} `json:"PlaceName,omitempty"`
			RequiredQuest interface{} `json:"RequiredQuest,omitempty"`
			Level3 interface{} `json:"Level3,omitempty"`
			Level0Target string `json:"Level0Target,omitempty"`
			Level2Target string `json:"Level2Target,omitempty"`
			Level3TargetID int `json:"Level3TargetID,omitempty"`
			PlaceNameTarget string `json:"PlaceNameTarget,omitempty"`
			TerritoryTarget string `json:"TerritoryTarget,omitempty"`
			AethernetGroup int `json:"AethernetGroup,omitempty"`
			MapTargetID int `json:"MapTargetID,omitempty"`
			AethernetNameTargetID int `json:"AethernetNameTargetID,omitempty"`
			IsAetheryte int `json:"IsAetheryte,omitempty"`
			Level0 interface{} `json:"Level0,omitempty"`
			Level1 interface{} `json:"Level1,omitempty"`
			RequiredQuestTargetID int `json:"RequiredQuestTargetID,omitempty"`
			AethernetName interface{} `json:"AethernetName,omitempty"`
			Level1Target string `json:"Level1Target,omitempty"`
			Level1TargetID int `json:"Level1TargetID,omitempty"`
			Level2 interface{} `json:"Level2,omitempty"`
			Map interface{} `json:"Map,omitempty"`
			MapTarget string `json:"MapTarget,omitempty"`
			RequiredQuestTarget string `json:"RequiredQuestTarget,omitempty"`
			Territory interface{} `json:"Territory,omitempty"`
			AetherstreamY int `json:"AetherstreamY,omitempty"`
			AetherstreamX int `json:"AetherstreamX,omitempty"`
			AethernetNameTarget string `json:"AethernetNameTarget,omitempty"`
			PlaceNameTargetID int `json:"PlaceNameTargetID,omitempty"`
			TerritoryTargetID int `json:"TerritoryTargetID,omitempty"`
			ID int `json:"ID,omitempty"`
			Level2TargetID int `json:"Level2TargetID,omitempty"`
			Level3Target string `json:"Level3Target,omitempty"`
			Level0TargetID int `json:"Level0TargetID,omitempty"`
		}
		WeatherRate int `json:"WeatherRate,omitempty"`
		PlaceNameRegion struct {
			Name_en string `json:"Name_en,omitempty"`
			ID int `json:"ID,omitempty"`
			Icon string `json:"Icon,omitempty"`
			Name string `json:"Name,omitempty"`
			NameNoArticle_en string `json:"NameNoArticle_en,omitempty"`
			Name_de string `json:"Name_de,omitempty"`
			Name_fr string `json:"Name_fr,omitempty"`
			Name_ja string `json:"Name_ja,omitempty"`
			NameNoArticle string `json:"NameNoArticle,omitempty"`
			NameNoArticle_de string `json:"NameNoArticle_de,omitempty"`
			NameNoArticle_fr string `json:"NameNoArticle_fr,omitempty"`
			NameNoArticle_ja string `json:"NameNoArticle_ja,omitempty"`
		}
	}
	TerritoryTargetID int `json:"TerritoryTargetID,omitempty"`
	Map struct {
		DiscoveryIndex int `json:"DiscoveryIndex,omitempty"`
		OffsetY int `json:"OffsetY,omitempty"`
		PlaceNameRegionTarget string `json:"PlaceNameRegionTarget,omitempty"`
		PlaceNameRegionTargetID int `json:"PlaceNameRegionTargetID,omitempty"`
		PlaceNameSub interface{} `json:"PlaceNameSub,omitempty"`
		PlaceNameSubTargetID int `json:"PlaceNameSubTargetID,omitempty"`
		TerritoryTypeTarget string `json:"TerritoryTypeTarget,omitempty"`
		OffsetX int `json:"OffsetX,omitempty"`
		PlaceNameRegion struct {
			ID int `json:"ID,omitempty"`
			Icon string `json:"Icon,omitempty"`
			Name string `json:"Name,omitempty"`
			NameNoArticle_en string `json:"NameNoArticle_en,omitempty"`
			NameNoArticle_fr string `json:"NameNoArticle_fr,omitempty"`
			Name_fr string `json:"Name_fr,omitempty"`
			NameNoArticle string `json:"NameNoArticle,omitempty"`
			NameNoArticle_de string `json:"NameNoArticle_de,omitempty"`
			NameNoArticle_ja string `json:"NameNoArticle_ja,omitempty"`
			Name_de string `json:"Name_de,omitempty"`
			Name_en string `json:"Name_en,omitempty"`
			Name_ja string `json:"Name_ja,omitempty"`
		}
		PlaceNameSubTarget string `json:"PlaceNameSubTarget,omitempty"`
		SizeFactor int `json:"SizeFactor,omitempty"`
		Hierarchy int `json:"Hierarchy,omitempty"`
		ID int `json:"ID,omitempty"`
		MapFilename string `json:"MapFilename,omitempty"`
		MapFilenameId string `json:"MapFilenameId,omitempty"`
		PlaceNameTargetID int `json:"PlaceNameTargetID,omitempty"`
		TerritoryType struct {
			TerritoryIntendedUse int `json:"TerritoryIntendedUse,omitempty"`
			MapTarget string `json:"MapTarget,omitempty"`
			PlaceNameZone interface{} `json:"PlaceNameZone,omitempty"`
			PlaceNameRegion interface{} `json:"PlaceNameRegion,omitempty"`
			PlaceNameRegionTargetID int `json:"PlaceNameRegionTargetID,omitempty"`
			WeatherRate int `json:"WeatherRate,omitempty"`
			Map interface{} `json:"Map,omitempty"`
			MapTargetID int `json:"MapTargetID,omitempty"`
			PlaceNameRegionTarget string `json:"PlaceNameRegionTarget,omitempty"`
			PlaceNameTargetID int `json:"PlaceNameTargetID,omitempty"`
			AetheryteTarget string `json:"AetheryteTarget,omitempty"`
			ArrayEventHandlerTarget string `json:"ArrayEventHandlerTarget,omitempty"`
			ArrayEventHandler interface{} `json:"ArrayEventHandler,omitempty"`
			ArrayEventHandlerTargetID int `json:"ArrayEventHandlerTargetID,omitempty"`
			Bg string `json:"Bg,omitempty"`
			Bg_en string `json:"Bg_en,omitempty"`
			ID int `json:"ID,omitempty"`
			Name string `json:"Name,omitempty"`
			Aetheryte interface{} `json:"Aetheryte,omitempty"`
			AetheryteTargetID int `json:"AetheryteTargetID,omitempty"`
			PlaceNameTarget string `json:"PlaceNameTarget,omitempty"`
			PlaceNameZoneTarget string `json:"PlaceNameZoneTarget,omitempty"`
			PlaceNameZoneTargetID int `json:"PlaceNameZoneTargetID,omitempty"`
			Name_en string `json:"Name_en,omitempty"`
			PlaceName interface{} `json:"PlaceName,omitempty"`
		}
		TerritoryTypeTargetID int `json:"TerritoryTypeTargetID,omitempty"`
		DiscoveryArrayByte int `json:"DiscoveryArrayByte,omitempty"`
		MapMarkerRange int `json:"MapMarkerRange,omitempty"`
		PlaceName struct {
			ID int `json:"ID,omitempty"`
			NameNoArticle_fr string `json:"NameNoArticle_fr,omitempty"`
			NameNoArticle_ja string `json:"NameNoArticle_ja,omitempty"`
			Name_en string `json:"Name_en,omitempty"`
			Name_fr string `json:"Name_fr,omitempty"`
			Name_ja string `json:"Name_ja,omitempty"`
			Icon string `json:"Icon,omitempty"`
			Name string `json:"Name,omitempty"`
			NameNoArticle string `json:"NameNoArticle,omitempty"`
			NameNoArticle_de string `json:"NameNoArticle_de,omitempty"`
			NameNoArticle_en string `json:"NameNoArticle_en,omitempty"`
			Name_de string `json:"Name_de,omitempty"`
		}
		PlaceNameTarget string `json:"PlaceNameTarget,omitempty"`
	}
}
