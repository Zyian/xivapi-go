// Code generated by generate_structs - DO NOT EDIT.

package xivapi

type QuickChat struct {
	ID int `json:"ID,omitempty"`
	NameAction string `json:"NameAction,omitempty"`
	NameAction_de string `json:"NameAction_de,omitempty"`
	NameAction_en string `json:"NameAction_en,omitempty"`
	TextOutput_ja string `json:"TextOutput_ja,omitempty"`
	Url string `json:"Url,omitempty"`
	AddonTarget string `json:"AddonTarget,omitempty"`
	GameContentLinks []interface{} `json:"GameContentLinks,omitempty"`
	NameAction_ja string `json:"NameAction_ja,omitempty"`
	QuickChatTransient struct {
		ID int `json:"ID,omitempty"`
		TextOutput string `json:"TextOutput,omitempty"`
		TextOutput_de string `json:"TextOutput_de,omitempty"`
		TextOutput_en string `json:"TextOutput_en,omitempty"`
		TextOutput_fr string `json:"TextOutput_fr,omitempty"`
		TextOutput_ja string `json:"TextOutput_ja,omitempty"`
	}
	QuickChatTransientTargetID int `json:"QuickChatTransientTargetID,omitempty"`
	TextOutput_fr string `json:"TextOutput_fr,omitempty"`
	Icon string `json:"Icon,omitempty"`
	QuickChatTransientTarget string `json:"QuickChatTransientTarget,omitempty"`
	TextOutput string `json:"TextOutput,omitempty"`
	TextOutput_de string `json:"TextOutput_de,omitempty"`
	Addon struct {
		Text string `json:"Text,omitempty"`
		Text_de string `json:"Text_de,omitempty"`
		Text_en string `json:"Text_en,omitempty"`
		Text_fr string `json:"Text_fr,omitempty"`
		Text_ja string `json:"Text_ja,omitempty"`
		ID int `json:"ID,omitempty"`
	}
	AddonTargetID int `json:"AddonTargetID,omitempty"`
	IconID int `json:"IconID,omitempty"`
	NameAction_fr string `json:"NameAction_fr,omitempty"`
	TextOutput_en string `json:"TextOutput_en,omitempty"`
}
