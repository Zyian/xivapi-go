// Code generated by generate_structs - DO NOT EDIT.

package xivapi

type PvPTrait struct {
	Trait1TargetID int `json:"Trait1TargetID,omitempty"`
	Trait2 interface{} `json:"Trait2,omitempty"`
	Trait2Target string `json:"Trait2Target,omitempty"`
	Trait3TargetID int `json:"Trait3TargetID,omitempty"`
	GameContentLinks []interface{} `json:"GameContentLinks,omitempty"`
	ID int `json:"ID,omitempty"`
	Trait1 interface{} `json:"Trait1,omitempty"`
	Trait1Target string `json:"Trait1Target,omitempty"`
	Trait2TargetID int `json:"Trait2TargetID,omitempty"`
	Trait3 interface{} `json:"Trait3,omitempty"`
	Trait3Target string `json:"Trait3Target,omitempty"`
	Url string `json:"Url,omitempty"`
}
