// Code generated by generate_structs - DO NOT EDIT.

package xivapi

type SpearfishingItem struct {
	Url string `json:"Url,omitempty"`
	Description_de string `json:"Description_de,omitempty"`
	GatheringItemLevel struct {
		ID int `json:"ID,omitempty"`
		Stars int `json:"Stars,omitempty"`
		GatheringItemLevel int `json:"GatheringItemLevel,omitempty"`
	}
	IsVisible int `json:"IsVisible,omitempty"`
	ItemTargetID int `json:"ItemTargetID,omitempty"`
	TerritoryType struct {
		ArrayEventHandlerTarget string `json:"ArrayEventHandlerTarget,omitempty"`
		MapTargetID int `json:"MapTargetID,omitempty"`
		Name string `json:"Name,omitempty"`
		PlaceNameRegionTargetID int `json:"PlaceNameRegionTargetID,omitempty"`
		AetheryteTargetID int `json:"AetheryteTargetID,omitempty"`
		ArrayEventHandler struct {
			Data1 int `json:"Data1,omitempty"`
			Data3 int `json:"Data3,omitempty"`
			Data5 int `json:"Data5,omitempty"`
			Data9 int `json:"Data9,omitempty"`
			ID int `json:"ID,omitempty"`
			Data11 int `json:"Data11,omitempty"`
			Data7 int `json:"Data7,omitempty"`
			Data8 int `json:"Data8,omitempty"`
			Data0 int `json:"Data0,omitempty"`
			Data12 int `json:"Data12,omitempty"`
			Data13 int `json:"Data13,omitempty"`
			Data15 int `json:"Data15,omitempty"`
			Data6 int `json:"Data6,omitempty"`
			Data10 int `json:"Data10,omitempty"`
			Data14 int `json:"Data14,omitempty"`
			Data2 int `json:"Data2,omitempty"`
			Data4 int `json:"Data4,omitempty"`
		}
		ArrayEventHandlerTargetID int `json:"ArrayEventHandlerTargetID,omitempty"`
		Bg string `json:"Bg,omitempty"`
		MapTarget string `json:"MapTarget,omitempty"`
		WeatherRate int `json:"WeatherRate,omitempty"`
		PlaceNameTarget string `json:"PlaceNameTarget,omitempty"`
		PlaceNameTargetID int `json:"PlaceNameTargetID,omitempty"`
		PlaceNameZoneTarget string `json:"PlaceNameZoneTarget,omitempty"`
		Aetheryte struct {
			AetherstreamY int `json:"AetherstreamY,omitempty"`
			Level2 interface{} `json:"Level2,omitempty"`
			Level3Target string `json:"Level3Target,omitempty"`
			Level3TargetID int `json:"Level3TargetID,omitempty"`
			RequiredQuestTarget string `json:"RequiredQuestTarget,omitempty"`
			AethernetName interface{} `json:"AethernetName,omitempty"`
			Level1TargetID int `json:"Level1TargetID,omitempty"`
			Level2Target string `json:"Level2Target,omitempty"`
			RequiredQuest interface{} `json:"RequiredQuest,omitempty"`
			Level0 interface{} `json:"Level0,omitempty"`
			PlaceName interface{} `json:"PlaceName,omitempty"`
			Territory interface{} `json:"Territory,omitempty"`
			PlaceNameTarget string `json:"PlaceNameTarget,omitempty"`
			RequiredQuestTargetID int `json:"RequiredQuestTargetID,omitempty"`
			AethernetNameTarget string `json:"AethernetNameTarget,omitempty"`
			AethernetNameTargetID int `json:"AethernetNameTargetID,omitempty"`
			AetherstreamX int `json:"AetherstreamX,omitempty"`
			ID int `json:"ID,omitempty"`
			Level0TargetID int `json:"Level0TargetID,omitempty"`
			MapTarget string `json:"MapTarget,omitempty"`
			IsAetheryte int `json:"IsAetheryte,omitempty"`
			Level0Target string `json:"Level0Target,omitempty"`
			Level1 interface{} `json:"Level1,omitempty"`
			PlaceNameTargetID int `json:"PlaceNameTargetID,omitempty"`
			Level1Target string `json:"Level1Target,omitempty"`
			Level2TargetID int `json:"Level2TargetID,omitempty"`
			Map interface{} `json:"Map,omitempty"`
			Level3 interface{} `json:"Level3,omitempty"`
			AethernetGroup int `json:"AethernetGroup,omitempty"`
			MapTargetID int `json:"MapTargetID,omitempty"`
			TerritoryTarget string `json:"TerritoryTarget,omitempty"`
			TerritoryTargetID int `json:"TerritoryTargetID,omitempty"`
		}
		AetheryteTarget string `json:"AetheryteTarget,omitempty"`
		Map struct {
			MapFilenameId string `json:"MapFilenameId,omitempty"`
			MapMarkerRange int `json:"MapMarkerRange,omitempty"`
			TerritoryType interface{} `json:"TerritoryType,omitempty"`
			DiscoveryArrayByte int `json:"DiscoveryArrayByte,omitempty"`
			ID int `json:"ID,omitempty"`
			OffsetX int `json:"OffsetX,omitempty"`
			OffsetY int `json:"OffsetY,omitempty"`
			PlaceNameSubTarget string `json:"PlaceNameSubTarget,omitempty"`
			PlaceNameTarget string `json:"PlaceNameTarget,omitempty"`
			SizeFactor int `json:"SizeFactor,omitempty"`
			PlaceName interface{} `json:"PlaceName,omitempty"`
			PlaceNameRegionTargetID int `json:"PlaceNameRegionTargetID,omitempty"`
			PlaceNameSub interface{} `json:"PlaceNameSub,omitempty"`
			PlaceNameTargetID int `json:"PlaceNameTargetID,omitempty"`
			TerritoryTypeTarget string `json:"TerritoryTypeTarget,omitempty"`
			TerritoryTypeTargetID int `json:"TerritoryTypeTargetID,omitempty"`
			DiscoveryIndex int `json:"DiscoveryIndex,omitempty"`
			Hierarchy int `json:"Hierarchy,omitempty"`
			MapFilename string `json:"MapFilename,omitempty"`
			PlaceNameRegion interface{} `json:"PlaceNameRegion,omitempty"`
			PlaceNameRegionTarget string `json:"PlaceNameRegionTarget,omitempty"`
			PlaceNameSubTargetID int `json:"PlaceNameSubTargetID,omitempty"`
		}
		PlaceName struct {
			NameNoArticle string `json:"NameNoArticle,omitempty"`
			NameNoArticle_en string `json:"NameNoArticle_en,omitempty"`
			NameNoArticle_fr string `json:"NameNoArticle_fr,omitempty"`
			NameNoArticle_ja string `json:"NameNoArticle_ja,omitempty"`
			Name_de string `json:"Name_de,omitempty"`
			Name_ja string `json:"Name_ja,omitempty"`
			ID int `json:"ID,omitempty"`
			Name string `json:"Name,omitempty"`
			NameNoArticle_de string `json:"NameNoArticle_de,omitempty"`
			Name_en string `json:"Name_en,omitempty"`
			Name_fr string `json:"Name_fr,omitempty"`
			Icon string `json:"Icon,omitempty"`
		}
		PlaceNameRegionTarget string `json:"PlaceNameRegionTarget,omitempty"`
		TerritoryIntendedUse int `json:"TerritoryIntendedUse,omitempty"`
		PlaceNameZoneTargetID int `json:"PlaceNameZoneTargetID,omitempty"`
		Bg_en string `json:"Bg_en,omitempty"`
		ID int `json:"ID,omitempty"`
		Name_en string `json:"Name_en,omitempty"`
		PlaceNameRegion struct {
			NameNoArticle_ja string `json:"NameNoArticle_ja,omitempty"`
			Name_de string `json:"Name_de,omitempty"`
			Name_en string `json:"Name_en,omitempty"`
			NameNoArticle string `json:"NameNoArticle,omitempty"`
			NameNoArticle_de string `json:"NameNoArticle_de,omitempty"`
			NameNoArticle_fr string `json:"NameNoArticle_fr,omitempty"`
			NameNoArticle_en string `json:"NameNoArticle_en,omitempty"`
			Name_fr string `json:"Name_fr,omitempty"`
			Name_ja string `json:"Name_ja,omitempty"`
			ID int `json:"ID,omitempty"`
			Icon string `json:"Icon,omitempty"`
			Name string `json:"Name,omitempty"`
		}
		PlaceNameZone struct {
			NameNoArticle string `json:"NameNoArticle,omitempty"`
			NameNoArticle_de string `json:"NameNoArticle_de,omitempty"`
			Name_en string `json:"Name_en,omitempty"`
			Name_ja string `json:"Name_ja,omitempty"`
			ID int `json:"ID,omitempty"`
			Name string `json:"Name,omitempty"`
			NameNoArticle_en string `json:"NameNoArticle_en,omitempty"`
			NameNoArticle_fr string `json:"NameNoArticle_fr,omitempty"`
			NameNoArticle_ja string `json:"NameNoArticle_ja,omitempty"`
			Name_de string `json:"Name_de,omitempty"`
			Name_fr string `json:"Name_fr,omitempty"`
			Icon string `json:"Icon,omitempty"`
		}
	}
	Description string `json:"Description,omitempty"`
	Description_fr string `json:"Description_fr,omitempty"`
	Description_ja string `json:"Description_ja,omitempty"`
	Item struct {
		BaseParamSpecial4 interface{} `json:"BaseParamSpecial4,omitempty"`
		DelayMs int `json:"DelayMs,omitempty"`
		Description string `json:"Description,omitempty"`
		FilterGroup int `json:"FilterGroup,omitempty"`
		IsCrestWorthy int `json:"IsCrestWorthy,omitempty"`
		Plural_ja string `json:"Plural_ja,omitempty"`
		BaseParam4Target string `json:"BaseParam4Target,omitempty"`
		CooldownS int `json:"CooldownS,omitempty"`
		Description_en string `json:"Description_en,omitempty"`
		ItemSeries interface{} `json:"ItemSeries,omitempty"`
		ItemSeriesTargetID int `json:"ItemSeriesTargetID,omitempty"`
		ItemSpecialBonus interface{} `json:"ItemSpecialBonus,omitempty"`
		Name_fr string `json:"Name_fr,omitempty"`
		ClassJobCategoryTarget string `json:"ClassJobCategoryTarget,omitempty"`
		BaseParamValue0 int `json:"BaseParamValue0,omitempty"`
		Salvage struct {
			ID int `json:"ID,omitempty"`
			OptimalSkill int `json:"OptimalSkill,omitempty"`
		}
		BaseParam3 interface{} `json:"BaseParam3,omitempty"`
		DamageMag int `json:"DamageMag,omitempty"`
		EquipSlotCategoryTarget string `json:"EquipSlotCategoryTarget,omitempty"`
		EquipSlotCategoryTargetID int `json:"EquipSlotCategoryTargetID,omitempty"`
		ItemUICategoryTarget string `json:"ItemUICategoryTarget,omitempty"`
		ClassJobUseTargetID int `json:"ClassJobUseTargetID,omitempty"`
		ItemRepairTargetID int `json:"ItemRepairTargetID,omitempty"`
		BaseParamValueSpecial5 int `json:"BaseParamValueSpecial5,omitempty"`
		ClassJobRepair struct {
			ID int `json:"ID,omitempty"`
			ItemStartingWeapon interface{} `json:"ItemStartingWeapon,omitempty"`
			NameEnglish_ja string `json:"NameEnglish_ja,omitempty"`
			LimitBreak2 interface{} `json:"LimitBreak2,omitempty"`
			ModifierStrength int `json:"ModifierStrength,omitempty"`
			NameEnglish_fr string `json:"NameEnglish_fr,omitempty"`
			ModifierVitality int `json:"ModifierVitality,omitempty"`
			NameEnglish string `json:"NameEnglish,omitempty"`
			ClassJobParentTarget string `json:"ClassJobParentTarget,omitempty"`
			ItemStartingWeaponTarget string `json:"ItemStartingWeaponTarget,omitempty"`
			LimitBreak2Target string `json:"LimitBreak2Target,omitempty"`
			LimitBreak2TargetID int `json:"LimitBreak2TargetID,omitempty"`
			ModifierIntelligence int `json:"ModifierIntelligence,omitempty"`
			ModifierMind int `json:"ModifierMind,omitempty"`
			LimitBreak1TargetID int `json:"LimitBreak1TargetID,omitempty"`
			ModifierHitPoints int `json:"ModifierHitPoints,omitempty"`
			PrerequisiteTargetID int `json:"PrerequisiteTargetID,omitempty"`
			RelicQuest interface{} `json:"RelicQuest,omitempty"`
			Name_ja string `json:"Name_ja,omitempty"`
			RelicQuestTarget string `json:"RelicQuestTarget,omitempty"`
			Abbreviation_fr string `json:"Abbreviation_fr,omitempty"`
			Abbreviation_ja string `json:"Abbreviation_ja,omitempty"`
			ClassJobCategoryTargetID int `json:"ClassJobCategoryTargetID,omitempty"`
			LimitBreak3Target string `json:"LimitBreak3Target,omitempty"`
			LimitBreak3TargetID int `json:"LimitBreak3TargetID,omitempty"`
			Name_fr string `json:"Name_fr,omitempty"`
			Abbreviation string `json:"Abbreviation,omitempty"`
			ClassJobCategory interface{} `json:"ClassJobCategory,omitempty"`
			ClassJobParent interface{} `json:"ClassJobParent,omitempty"`
			ItemSoulCrystalTargetID int `json:"ItemSoulCrystalTargetID,omitempty"`
			LimitBreak3 interface{} `json:"LimitBreak3,omitempty"`
			ModifierManaPoints int `json:"ModifierManaPoints,omitempty"`
			LimitBreak1Target string `json:"LimitBreak1Target,omitempty"`
			ModifierDexterity int `json:"ModifierDexterity,omitempty"`
			ModifierPiety int `json:"ModifierPiety,omitempty"`
			Name_en string `json:"Name_en,omitempty"`
			Abbreviation_en string `json:"Abbreviation_en,omitempty"`
			ClassJobCategoryTarget string `json:"ClassJobCategoryTarget,omitempty"`
			RelicQuestTargetID int `json:"RelicQuestTargetID,omitempty"`
			StartingLevel int `json:"StartingLevel,omitempty"`
			ClassJobParentTargetID int `json:"ClassJobParentTargetID,omitempty"`
			ItemSoulCrystal interface{} `json:"ItemSoulCrystal,omitempty"`
			Name_de string `json:"Name_de,omitempty"`
			Prerequisite interface{} `json:"Prerequisite,omitempty"`
			ExpArrayIndex int `json:"ExpArrayIndex,omitempty"`
			Name string `json:"Name,omitempty"`
			NameEnglish_en string `json:"NameEnglish_en,omitempty"`
			UnlockQuest interface{} `json:"UnlockQuest,omitempty"`
			ItemSoulCrystalTarget string `json:"ItemSoulCrystalTarget,omitempty"`
			NameEnglish_de string `json:"NameEnglish_de,omitempty"`
			UnlockQuestTarget string `json:"UnlockQuestTarget,omitempty"`
			Icon string `json:"Icon,omitempty"`
			PrerequisiteTarget string `json:"PrerequisiteTarget,omitempty"`
			Abbreviation_de string `json:"Abbreviation_de,omitempty"`
			ItemStartingWeaponTargetID int `json:"ItemStartingWeaponTargetID,omitempty"`
			LimitBreak1 interface{} `json:"LimitBreak1,omitempty"`
			UnlockQuestTargetID int `json:"UnlockQuestTargetID,omitempty"`
		}
		IconID int `json:"IconID,omitempty"`
		BaseParamValueSpecial1 int `json:"BaseParamValueSpecial1,omitempty"`
		ItemAction interface{} `json:"ItemAction,omitempty"`
		BaseParamValue3 int `json:"BaseParamValue3,omitempty"`
		BaseParamValueSpecial2 int `json:"BaseParamValueSpecial2,omitempty"`
		EquipRestriction int `json:"EquipRestriction,omitempty"`
		IsAdvancedMeldingPermitted int `json:"IsAdvancedMeldingPermitted,omitempty"`
		IsUntradable int `json:"IsUntradable,omitempty"`
		ItemSpecialBonusTargetID int `json:"ItemSpecialBonusTargetID,omitempty"`
		LevelEquip int `json:"LevelEquip,omitempty"`
		BaseParamSpecial4Target string `json:"BaseParamSpecial4Target,omitempty"`
		BaseParamSpecial3TargetID int `json:"BaseParamSpecial3TargetID,omitempty"`
		ClassJobRepairTarget string `json:"ClassJobRepairTarget,omitempty"`
		Name_ja string `json:"Name_ja,omitempty"`
		BaseParamSpecial1Target string `json:"BaseParamSpecial1Target,omitempty"`
		ItemGlamourTarget string `json:"ItemGlamourTarget,omitempty"`
		Name string `json:"Name,omitempty"`
		BaseParam3TargetID int `json:"BaseParam3TargetID,omitempty"`
		BaseParamValueSpecial4 int `json:"BaseParamValueSpecial4,omitempty"`
		BaseParamValueSpecial0 int `json:"BaseParamValueSpecial0,omitempty"`
		ItemSeriesTarget string `json:"ItemSeriesTarget,omitempty"`
		BaseParam2TargetID int `json:"BaseParam2TargetID,omitempty"`
		BlockRate int `json:"BlockRate,omitempty"`
		BaseParamSpecial2 interface{} `json:"BaseParamSpecial2,omitempty"`
		EquipSlotCategory interface{} `json:"EquipSlotCategory,omitempty"`
		ItemSearchCategoryTarget string `json:"ItemSearchCategoryTarget,omitempty"`
		ItemUICategoryTargetID int `json:"ItemUICategoryTargetID,omitempty"`
		Plural_de string `json:"Plural_de,omitempty"`
		Rarity int `json:"Rarity,omitempty"`
		BaseParamSpecial3Target string `json:"BaseParamSpecial3Target,omitempty"`
		DamagePhys int `json:"DamagePhys,omitempty"`
		Description_ja string `json:"Description_ja,omitempty"`
		GrandCompanyTarget string `json:"GrandCompanyTarget,omitempty"`
		ItemSpecialBonusTarget string `json:"ItemSpecialBonusTarget,omitempty"`
		ItemUICategory struct {
			IconID int `json:"IconID,omitempty"`
			Name string `json:"Name,omitempty"`
			Name_de string `json:"Name_de,omitempty"`
			Name_en string `json:"Name_en,omitempty"`
			Name_fr string `json:"Name_fr,omitempty"`
			OrderMajor int `json:"OrderMajor,omitempty"`
			ID int `json:"ID,omitempty"`
			Icon string `json:"Icon,omitempty"`
			Name_ja string `json:"Name_ja,omitempty"`
			OrderMinor int `json:"OrderMinor,omitempty"`
		}
		BaseParam1Target string `json:"BaseParam1Target,omitempty"`
		Block int `json:"Block,omitempty"`
		DefensePhys int `json:"DefensePhys,omitempty"`
		ModelSub string `json:"ModelSub,omitempty"`
		PriceLow int `json:"PriceLow,omitempty"`
		BaseParamSpecial2Target string `json:"BaseParamSpecial2Target,omitempty"`
		IsPvP int `json:"IsPvP,omitempty"`
		MaterializeType int `json:"MaterializeType,omitempty"`
		Singular_ja string `json:"Singular_ja,omitempty"`
		BaseParam5 interface{} `json:"BaseParam5,omitempty"`
		GrandCompany interface{} `json:"GrandCompany,omitempty"`
		Plural_fr string `json:"Plural_fr,omitempty"`
		Singular string `json:"Singular,omitempty"`
		BaseParamValue5 int `json:"BaseParamValue5,omitempty"`
		BaseParamSpecial5Target string `json:"BaseParamSpecial5Target,omitempty"`
		ClassJobUse interface{} `json:"ClassJobUse,omitempty"`
		Icon string `json:"Icon,omitempty"`
		BaseParamSpecial4TargetID int `json:"BaseParamSpecial4TargetID,omitempty"`
		ClassJobUseTarget string `json:"ClassJobUseTarget,omitempty"`
		IsCollectable int `json:"IsCollectable,omitempty"`
		ModelMain string `json:"ModelMain,omitempty"`
		Name_de string `json:"Name_de,omitempty"`
		BaseParam2 interface{} `json:"BaseParam2,omitempty"`
		BaseParamSpecial3 interface{} `json:"BaseParamSpecial3,omitempty"`
		BaseParamSpecial5 interface{} `json:"BaseParamSpecial5,omitempty"`
		IsEquippable int `json:"IsEquippable,omitempty"`
		ItemActionTarget string `json:"ItemActionTarget,omitempty"`
		StartsWithVowel int `json:"StartsWithVowel,omitempty"`
		AetherialReduce int `json:"AetherialReduce,omitempty"`
		Description_fr string `json:"Description_fr,omitempty"`
		IsIndisposable int `json:"IsIndisposable,omitempty"`
		SalvageTarget string `json:"SalvageTarget,omitempty"`
		StackSize int `json:"StackSize,omitempty"`
		BaseParam1 interface{} `json:"BaseParam1,omitempty"`
		BaseParamSpecial0 interface{} `json:"BaseParamSpecial0,omitempty"`
		DefenseMag int `json:"DefenseMag,omitempty"`
		GrandCompanyTargetID int `json:"GrandCompanyTargetID,omitempty"`
		BaseParamModifier int `json:"BaseParamModifier,omitempty"`
		BaseParam4 interface{} `json:"BaseParam4,omitempty"`
		CanBeHq int `json:"CanBeHq,omitempty"`
		Singular_de string `json:"Singular_de,omitempty"`
		BaseParam0TargetID int `json:"BaseParam0TargetID,omitempty"`
		BaseParamSpecial0TargetID int `json:"BaseParamSpecial0TargetID,omitempty"`
		BaseParamSpecial1TargetID int `json:"BaseParamSpecial1TargetID,omitempty"`
		BaseParamValue2 int `json:"BaseParamValue2,omitempty"`
		BaseParamValue4 int `json:"BaseParamValue4,omitempty"`
		IsDyeable int `json:"IsDyeable,omitempty"`
		Name_en string `json:"Name_en,omitempty"`
		BaseParam2Target string `json:"BaseParam2Target,omitempty"`
		ClassJobRepairTargetID int `json:"ClassJobRepairTargetID,omitempty"`
		ItemGlamourTargetID int `json:"ItemGlamourTargetID,omitempty"`
		LevelItem int `json:"LevelItem,omitempty"`
		MateriaSlotCount int `json:"MateriaSlotCount,omitempty"`
		Plural string `json:"Plural,omitempty"`
		Plural_en string `json:"Plural_en,omitempty"`
		BaseParamSpecial2TargetID int `json:"BaseParamSpecial2TargetID,omitempty"`
		BaseParamValueSpecial3 int `json:"BaseParamValueSpecial3,omitempty"`
		Description_de string `json:"Description_de,omitempty"`
		ItemRepair interface{} `json:"ItemRepair,omitempty"`
		BaseParam5TargetID int `json:"BaseParam5TargetID,omitempty"`
		SalvageTargetID int `json:"SalvageTargetID,omitempty"`
		BaseParam0Target string `json:"BaseParam0Target,omitempty"`
		BaseParam4TargetID int `json:"BaseParam4TargetID,omitempty"`
		BaseParamSpecial1 interface{} `json:"BaseParamSpecial1,omitempty"`
		ClassJobCategory interface{} `json:"ClassJobCategory,omitempty"`
		ItemSearchCategory struct {
			Name string `json:"Name,omitempty"`
			Name_en string `json:"Name_en,omitempty"`
			Category int `json:"Category,omitempty"`
			ClassJob interface{} `json:"ClassJob,omitempty"`
			ClassJobTarget string `json:"ClassJobTarget,omitempty"`
			ClassJobTargetID int `json:"ClassJobTargetID,omitempty"`
			ID int `json:"ID,omitempty"`
			Icon string `json:"Icon,omitempty"`
			Name_fr string `json:"Name_fr,omitempty"`
			Name_ja string `json:"Name_ja,omitempty"`
			Order int `json:"Order,omitempty"`
			IconID int `json:"IconID,omitempty"`
			Name_de string `json:"Name_de,omitempty"`
		}
		ItemSearchCategoryTargetID int `json:"ItemSearchCategoryTargetID,omitempty"`
		BaseParam0 interface{} `json:"BaseParam0,omitempty"`
		IsUnique int `json:"IsUnique,omitempty"`
		ItemRepairTarget string `json:"ItemRepairTarget,omitempty"`
		ItemSpecialBonusParam int `json:"ItemSpecialBonusParam,omitempty"`
		Singular_fr string `json:"Singular_fr,omitempty"`
		BaseParamValue1 int `json:"BaseParamValue1,omitempty"`
		BaseParamSpecial0Target string `json:"BaseParamSpecial0Target,omitempty"`
		ClassJobCategoryTargetID int `json:"ClassJobCategoryTargetID,omitempty"`
		ID int `json:"ID,omitempty"`
		IsGlamourous int `json:"IsGlamourous,omitempty"`
		ItemActionTargetID int `json:"ItemActionTargetID,omitempty"`
		PriceMid int `json:"PriceMid,omitempty"`
		Singular_en string `json:"Singular_en,omitempty"`
		BaseParam1TargetID int `json:"BaseParam1TargetID,omitempty"`
		BaseParam3Target string `json:"BaseParam3Target,omitempty"`
		BaseParam5Target string `json:"BaseParam5Target,omitempty"`
		BaseParamSpecial5TargetID int `json:"BaseParamSpecial5TargetID,omitempty"`
		ItemGlamour interface{} `json:"ItemGlamour,omitempty"`
		AdditionalData int `json:"AdditionalData,omitempty"`
	}
	Description_en string `json:"Description_en,omitempty"`
	GatheringItemLevelTarget string `json:"GatheringItemLevelTarget,omitempty"`
	GatheringItemLevelTargetID int `json:"GatheringItemLevelTargetID,omitempty"`
	ID int `json:"ID,omitempty"`
	TerritoryTypeTarget string `json:"TerritoryTypeTarget,omitempty"`
	GameContentLinks []interface{} `json:"GameContentLinks,omitempty"`
	ItemTarget string `json:"ItemTarget,omitempty"`
	TerritoryTypeTargetID int `json:"TerritoryTypeTargetID,omitempty"`
}
