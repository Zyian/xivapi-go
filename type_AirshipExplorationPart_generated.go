// Code generated by generate_structs - DO NOT EDIT.

package xivapi

type AirshipExplorationPart struct {
	Components int `json:"Components,omitempty"`
	Favor int `json:"Favor,omitempty"`
	GameContentLinks []interface{} `json:"GameContentLinks,omitempty"`
	ID int `json:"ID,omitempty"`
	Surveillance int `json:"Surveillance,omitempty"`
	Url string `json:"Url,omitempty"`
	Range int `json:"Range,omitempty"`
	Rank int `json:"Rank,omitempty"`
	RepairMaterials int `json:"RepairMaterials,omitempty"`
	Retrieval int `json:"Retrieval,omitempty"`
	Speed int `json:"Speed,omitempty"`
}
