// Code generated by generate_structs - DO NOT EDIT.

package xivapi

type SatisfactionSupply struct {
	ID int `json:"ID,omitempty"`
	Item struct {
		ClassJobUseTarget string `json:"ClassJobUseTarget,omitempty"`
		Description_de string `json:"Description_de,omitempty"`
		FilterGroup int `json:"FilterGroup,omitempty"`
		IsUntradable int `json:"IsUntradable,omitempty"`
		BaseParamValue0 int `json:"BaseParamValue0,omitempty"`
		DelayMs int `json:"DelayMs,omitempty"`
		Description string `json:"Description,omitempty"`
		Icon string `json:"Icon,omitempty"`
		IsDyeable int `json:"IsDyeable,omitempty"`
		ItemUICategoryTargetID int `json:"ItemUICategoryTargetID,omitempty"`
		Singular_fr string `json:"Singular_fr,omitempty"`
		BaseParamSpecial5 interface{} `json:"BaseParamSpecial5,omitempty"`
		ItemRepairTarget string `json:"ItemRepairTarget,omitempty"`
		Name_de string `json:"Name_de,omitempty"`
		BaseParam2TargetID int `json:"BaseParam2TargetID,omitempty"`
		BlockRate int `json:"BlockRate,omitempty"`
		GrandCompany interface{} `json:"GrandCompany,omitempty"`
		ItemActionTargetID int `json:"ItemActionTargetID,omitempty"`
		ItemSpecialBonusParam int `json:"ItemSpecialBonusParam,omitempty"`
		ItemSpecialBonusTargetID int `json:"ItemSpecialBonusTargetID,omitempty"`
		Plural string `json:"Plural,omitempty"`
		Plural_de string `json:"Plural_de,omitempty"`
		BaseParamSpecial1TargetID int `json:"BaseParamSpecial1TargetID,omitempty"`
		SalvageTargetID int `json:"SalvageTargetID,omitempty"`
		Rarity int `json:"Rarity,omitempty"`
		ModelSub string `json:"ModelSub,omitempty"`
		ClassJobUseTargetID int `json:"ClassJobUseTargetID,omitempty"`
		BaseParam3TargetID int `json:"BaseParam3TargetID,omitempty"`
		BaseParamValue1 int `json:"BaseParamValue1,omitempty"`
		DefenseMag int `json:"DefenseMag,omitempty"`
		ItemAction interface{} `json:"ItemAction,omitempty"`
		ItemSeriesTargetID int `json:"ItemSeriesTargetID,omitempty"`
		BaseParam3 interface{} `json:"BaseParam3,omitempty"`
		EquipSlotCategoryTargetID int `json:"EquipSlotCategoryTargetID,omitempty"`
		IsAdvancedMeldingPermitted int `json:"IsAdvancedMeldingPermitted,omitempty"`
		BaseParam0TargetID int `json:"BaseParam0TargetID,omitempty"`
		BaseParamValue5 int `json:"BaseParamValue5,omitempty"`
		DefensePhys int `json:"DefensePhys,omitempty"`
		Description_en string `json:"Description_en,omitempty"`
		ItemGlamour interface{} `json:"ItemGlamour,omitempty"`
		BaseParamSpecial0TargetID int `json:"BaseParamSpecial0TargetID,omitempty"`
		BaseParam4Target string `json:"BaseParam4Target,omitempty"`
		BaseParamSpecial0 interface{} `json:"BaseParamSpecial0,omitempty"`
		Description_fr string `json:"Description_fr,omitempty"`
		GrandCompanyTarget string `json:"GrandCompanyTarget,omitempty"`
		ItemSearchCategoryTargetID int `json:"ItemSearchCategoryTargetID,omitempty"`
		ItemSeries interface{} `json:"ItemSeries,omitempty"`
		BaseParam0Target string `json:"BaseParam0Target,omitempty"`
		BaseParam4TargetID int `json:"BaseParam4TargetID,omitempty"`
		BaseParamValue2 int `json:"BaseParamValue2,omitempty"`
		ItemSpecialBonusTarget string `json:"ItemSpecialBonusTarget,omitempty"`
		MateriaSlotCount int `json:"MateriaSlotCount,omitempty"`
		BaseParam1TargetID int `json:"BaseParam1TargetID,omitempty"`
		ItemRepair interface{} `json:"ItemRepair,omitempty"`
		Name_fr string `json:"Name_fr,omitempty"`
		ClassJobUse interface{} `json:"ClassJobUse,omitempty"`
		BaseParamSpecial0Target string `json:"BaseParamSpecial0Target,omitempty"`
		BaseParamSpecial2TargetID int `json:"BaseParamSpecial2TargetID,omitempty"`
		BaseParamSpecial3 interface{} `json:"BaseParamSpecial3,omitempty"`
		BaseParamValueSpecial2 int `json:"BaseParamValueSpecial2,omitempty"`
		BaseParamValueSpecial5 int `json:"BaseParamValueSpecial5,omitempty"`
		ClassJobCategory interface{} `json:"ClassJobCategory,omitempty"`
		DamagePhys int `json:"DamagePhys,omitempty"`
		AdditionalData int `json:"AdditionalData,omitempty"`
		Plural_ja string `json:"Plural_ja,omitempty"`
		ItemSearchCategoryTarget string `json:"ItemSearchCategoryTarget,omitempty"`
		EquipRestriction int `json:"EquipRestriction,omitempty"`
		EquipSlotCategory interface{} `json:"EquipSlotCategory,omitempty"`
		LevelEquip int `json:"LevelEquip,omitempty"`
		Name_en string `json:"Name_en,omitempty"`
		Singular_en string `json:"Singular_en,omitempty"`
		StackSize int `json:"StackSize,omitempty"`
		Block int `json:"Block,omitempty"`
		BaseParamSpecial2Target string `json:"BaseParamSpecial2Target,omitempty"`
		Description_ja string `json:"Description_ja,omitempty"`
		ItemGlamourTarget string `json:"ItemGlamourTarget,omitempty"`
		PriceLow int `json:"PriceLow,omitempty"`
		BaseParam1 interface{} `json:"BaseParam1,omitempty"`
		BaseParamValueSpecial3 int `json:"BaseParamValueSpecial3,omitempty"`
		EquipSlotCategoryTarget string `json:"EquipSlotCategoryTarget,omitempty"`
		ItemGlamourTargetID int `json:"ItemGlamourTargetID,omitempty"`
		LevelItem int `json:"LevelItem,omitempty"`
		Singular string `json:"Singular,omitempty"`
		BaseParam5 interface{} `json:"BaseParam5,omitempty"`
		BaseParamValueSpecial4 int `json:"BaseParamValueSpecial4,omitempty"`
		ClassJobCategoryTarget string `json:"ClassJobCategoryTarget,omitempty"`
		PriceMid int `json:"PriceMid,omitempty"`
		BaseParamModifier int `json:"BaseParamModifier,omitempty"`
		ClassJobRepairTargetID int `json:"ClassJobRepairTargetID,omitempty"`
		IsGlamourous int `json:"IsGlamourous,omitempty"`
		BaseParam2Target string `json:"BaseParam2Target,omitempty"`
		BaseParamSpecial3Target string `json:"BaseParamSpecial3Target,omitempty"`
		IsPvP int `json:"IsPvP,omitempty"`
		ItemSeriesTarget string `json:"ItemSeriesTarget,omitempty"`
		StartsWithVowel int `json:"StartsWithVowel,omitempty"`
		BaseParam2 interface{} `json:"BaseParam2,omitempty"`
		Plural_fr string `json:"Plural_fr,omitempty"`
		ModelMain string `json:"ModelMain,omitempty"`
		BaseParamValue4 int `json:"BaseParamValue4,omitempty"`
		ClassJobRepair interface{} `json:"ClassJobRepair,omitempty"`
		BaseParam5TargetID int `json:"BaseParam5TargetID,omitempty"`
		CooldownS int `json:"CooldownS,omitempty"`
		BaseParamSpecial4Target string `json:"BaseParamSpecial4Target,omitempty"`
		BaseParamSpecial1 interface{} `json:"BaseParamSpecial1,omitempty"`
		BaseParamSpecial4 interface{} `json:"BaseParamSpecial4,omitempty"`
		DamageMag int `json:"DamageMag,omitempty"`
		IsCollectable int `json:"IsCollectable,omitempty"`
		SalvageTarget string `json:"SalvageTarget,omitempty"`
		BaseParam3Target string `json:"BaseParam3Target,omitempty"`
		Singular_ja string `json:"Singular_ja,omitempty"`
		IconID int `json:"IconID,omitempty"`
		BaseParam5Target string `json:"BaseParam5Target,omitempty"`
		BaseParamSpecial1Target string `json:"BaseParamSpecial1Target,omitempty"`
		GrandCompanyTargetID int `json:"GrandCompanyTargetID,omitempty"`
		ItemUICategoryTarget string `json:"ItemUICategoryTarget,omitempty"`
		Name string `json:"Name,omitempty"`
		BaseParam0 interface{} `json:"BaseParam0,omitempty"`
		CanBeHq int `json:"CanBeHq,omitempty"`
		ClassJobCategoryTargetID int `json:"ClassJobCategoryTargetID,omitempty"`
		ID int `json:"ID,omitempty"`
		IsEquippable int `json:"IsEquippable,omitempty"`
		IsIndisposable int `json:"IsIndisposable,omitempty"`
		Plural_en string `json:"Plural_en,omitempty"`
		BaseParamSpecial3TargetID int `json:"BaseParamSpecial3TargetID,omitempty"`
		ItemSearchCategory interface{} `json:"ItemSearchCategory,omitempty"`
		ItemUICategory struct {
			Name_ja string `json:"Name_ja,omitempty"`
			ID int `json:"ID,omitempty"`
			Icon string `json:"Icon,omitempty"`
			Name string `json:"Name,omitempty"`
			Name_en string `json:"Name_en,omitempty"`
			Name_fr string `json:"Name_fr,omitempty"`
			IconID int `json:"IconID,omitempty"`
			Name_de string `json:"Name_de,omitempty"`
			OrderMajor int `json:"OrderMajor,omitempty"`
			OrderMinor int `json:"OrderMinor,omitempty"`
		}
		Name_ja string `json:"Name_ja,omitempty"`
		BaseParamSpecial2 interface{} `json:"BaseParamSpecial2,omitempty"`
		IsUnique int `json:"IsUnique,omitempty"`
		ItemActionTarget string `json:"ItemActionTarget,omitempty"`
		IsCrestWorthy int `json:"IsCrestWorthy,omitempty"`
		BaseParam4 interface{} `json:"BaseParam4,omitempty"`
		BaseParamSpecial4TargetID int `json:"BaseParamSpecial4TargetID,omitempty"`
		AetherialReduce int `json:"AetherialReduce,omitempty"`
		BaseParamSpecial5Target string `json:"BaseParamSpecial5Target,omitempty"`
		BaseParamValueSpecial1 int `json:"BaseParamValueSpecial1,omitempty"`
		ItemRepairTargetID int `json:"ItemRepairTargetID,omitempty"`
		Salvage interface{} `json:"Salvage,omitempty"`
		Singular_de string `json:"Singular_de,omitempty"`
		BaseParam1Target string `json:"BaseParam1Target,omitempty"`
		ClassJobRepairTarget string `json:"ClassJobRepairTarget,omitempty"`
		MaterializeType int `json:"MaterializeType,omitempty"`
		BaseParamValue3 int `json:"BaseParamValue3,omitempty"`
		BaseParamValueSpecial0 int `json:"BaseParamValueSpecial0,omitempty"`
		ItemSpecialBonus interface{} `json:"ItemSpecialBonus,omitempty"`
		BaseParamSpecial5TargetID int `json:"BaseParamSpecial5TargetID,omitempty"`
	}
	ItemTarget string `json:"ItemTarget,omitempty"`
	ItemTargetID int `json:"ItemTargetID,omitempty"`
	RewardTargetID int `json:"RewardTargetID,omitempty"`
	Slot int `json:"Slot,omitempty"`
	ProbabilityPerc int `json:"Probability%,omitempty"`
	RewardTarget string `json:"RewardTarget,omitempty"`
	CollectabilityHigh int `json:"CollectabilityHigh,omitempty"`
	CollectabilityLow int `json:"CollectabilityLow,omitempty"`
	CollectabilityMid int `json:"CollectabilityMid,omitempty"`
	GameContentLinks []interface{} `json:"GameContentLinks,omitempty"`
	Reward struct {
		RewardCurrency0TargetID int `json:"RewardCurrency0TargetID,omitempty"`
		ID int `json:"ID,omitempty"`
		QuantityHigh0 int `json:"QuantityHigh0,omitempty"`
		QuantityLow0 int `json:"QuantityLow0,omitempty"`
		QuantityMid1 int `json:"QuantityMid1,omitempty"`
		RewardCurrency0 struct {
			ID int `json:"ID,omitempty"`
			Item interface{} `json:"Item,omitempty"`
			ItemTarget string `json:"ItemTarget,omitempty"`
			ItemTargetID int `json:"ItemTargetID,omitempty"`
			Limit int `json:"Limit,omitempty"`
		}
		SatisfactionMid int `json:"SatisfactionMid,omitempty"`
		GilMid int `json:"GilMid,omitempty"`
		QuantityHigh1 int `json:"QuantityHigh1,omitempty"`
		RewardCurrency1TargetID int `json:"RewardCurrency1TargetID,omitempty"`
		SatisfactionHigh int `json:"SatisfactionHigh,omitempty"`
		SatisfactionLow int `json:"SatisfactionLow,omitempty"`
		GilLow int `json:"GilLow,omitempty"`
		RewardCurrency1Target string `json:"RewardCurrency1Target,omitempty"`
		QuantityMid0 int `json:"QuantityMid0,omitempty"`
		RewardCurrency0Target string `json:"RewardCurrency0Target,omitempty"`
		RewardCurrency1 struct {
			ID int `json:"ID,omitempty"`
			Item interface{} `json:"Item,omitempty"`
			ItemTarget string `json:"ItemTarget,omitempty"`
			ItemTargetID int `json:"ItemTargetID,omitempty"`
			Limit int `json:"Limit,omitempty"`
		}
		GilHigh int `json:"GilHigh,omitempty"`
		QuantityLow1 int `json:"QuantityLow1,omitempty"`
	}
	Url string `json:"Url,omitempty"`
}
