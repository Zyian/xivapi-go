// Code generated by generate_structs - DO NOT EDIT.

package xivapi

type SpearfishingNotebook struct {
	GameContentLinks []interface{} `json:"GameContentLinks,omitempty"`
	PlaceNameTargetID int `json:"PlaceNameTargetID,omitempty"`
	Radius int `json:"Radius,omitempty"`
	GatheringPointBaseTarget string `json:"GatheringPointBaseTarget,omitempty"`
	ID int `json:"ID,omitempty"`
	TerritoryType struct {
		MapTarget string `json:"MapTarget,omitempty"`
		MapTargetID int `json:"MapTargetID,omitempty"`
		PlaceName struct {
			NameNoArticle_de string `json:"NameNoArticle_de,omitempty"`
			NameNoArticle_en string `json:"NameNoArticle_en,omitempty"`
			NameNoArticle_ja string `json:"NameNoArticle_ja,omitempty"`
			Name_en string `json:"Name_en,omitempty"`
			Name_fr string `json:"Name_fr,omitempty"`
			Name_ja string `json:"Name_ja,omitempty"`
			Icon string `json:"Icon,omitempty"`
			Name string `json:"Name,omitempty"`
			NameNoArticle_fr string `json:"NameNoArticle_fr,omitempty"`
			Name_de string `json:"Name_de,omitempty"`
			ID int `json:"ID,omitempty"`
			NameNoArticle string `json:"NameNoArticle,omitempty"`
		}
		PlaceNameRegionTarget string `json:"PlaceNameRegionTarget,omitempty"`
		PlaceNameZoneTarget string `json:"PlaceNameZoneTarget,omitempty"`
		ArrayEventHandlerTargetID int `json:"ArrayEventHandlerTargetID,omitempty"`
		Bg string `json:"Bg,omitempty"`
		ID int `json:"ID,omitempty"`
		PlaceNameZoneTargetID int `json:"PlaceNameZoneTargetID,omitempty"`
		WeatherRate int `json:"WeatherRate,omitempty"`
		AetheryteTargetID int `json:"AetheryteTargetID,omitempty"`
		ArrayEventHandlerTarget string `json:"ArrayEventHandlerTarget,omitempty"`
		PlaceNameTargetID int `json:"PlaceNameTargetID,omitempty"`
		Name_en string `json:"Name_en,omitempty"`
		PlaceNameRegion struct {
			ID int `json:"ID,omitempty"`
			Icon string `json:"Icon,omitempty"`
			NameNoArticle_de string `json:"NameNoArticle_de,omitempty"`
			NameNoArticle_ja string `json:"NameNoArticle_ja,omitempty"`
			Name_fr string `json:"Name_fr,omitempty"`
			Name_ja string `json:"Name_ja,omitempty"`
			Name string `json:"Name,omitempty"`
			NameNoArticle string `json:"NameNoArticle,omitempty"`
			NameNoArticle_en string `json:"NameNoArticle_en,omitempty"`
			NameNoArticle_fr string `json:"NameNoArticle_fr,omitempty"`
			Name_de string `json:"Name_de,omitempty"`
			Name_en string `json:"Name_en,omitempty"`
		}
		PlaceNameRegionTargetID int `json:"PlaceNameRegionTargetID,omitempty"`
		TerritoryIntendedUse int `json:"TerritoryIntendedUse,omitempty"`
		Aetheryte struct {
			AethernetNameTarget string `json:"AethernetNameTarget,omitempty"`
			Level0Target string `json:"Level0Target,omitempty"`
			TerritoryTarget string `json:"TerritoryTarget,omitempty"`
			AethernetGroup int `json:"AethernetGroup,omitempty"`
			AethernetName interface{} `json:"AethernetName,omitempty"`
			AetherstreamX int `json:"AetherstreamX,omitempty"`
			ID int `json:"ID,omitempty"`
			Level2Target string `json:"Level2Target,omitempty"`
			Level3Target string `json:"Level3Target,omitempty"`
			Map interface{} `json:"Map,omitempty"`
			MapTarget string `json:"MapTarget,omitempty"`
			PlaceNameTarget string `json:"PlaceNameTarget,omitempty"`
			AethernetNameTargetID int `json:"AethernetNameTargetID,omitempty"`
			IsAetheryte int `json:"IsAetheryte,omitempty"`
			Level1 interface{} `json:"Level1,omitempty"`
			MapTargetID int `json:"MapTargetID,omitempty"`
			PlaceNameTargetID int `json:"PlaceNameTargetID,omitempty"`
			RequiredQuest interface{} `json:"RequiredQuest,omitempty"`
			TerritoryTargetID int `json:"TerritoryTargetID,omitempty"`
			Level0TargetID int `json:"Level0TargetID,omitempty"`
			Level3 interface{} `json:"Level3,omitempty"`
			RequiredQuestTarget string `json:"RequiredQuestTarget,omitempty"`
			Level1Target string `json:"Level1Target,omitempty"`
			AetherstreamY int `json:"AetherstreamY,omitempty"`
			Level2 interface{} `json:"Level2,omitempty"`
			Level2TargetID int `json:"Level2TargetID,omitempty"`
			PlaceName interface{} `json:"PlaceName,omitempty"`
			RequiredQuestTargetID int `json:"RequiredQuestTargetID,omitempty"`
			Territory interface{} `json:"Territory,omitempty"`
			Level0 interface{} `json:"Level0,omitempty"`
			Level1TargetID int `json:"Level1TargetID,omitempty"`
			Level3TargetID int `json:"Level3TargetID,omitempty"`
		}
		Map struct {
			DiscoveryIndex int `json:"DiscoveryIndex,omitempty"`
			ID int `json:"ID,omitempty"`
			MapFilename string `json:"MapFilename,omitempty"`
			OffsetY int `json:"OffsetY,omitempty"`
			PlaceNameRegion interface{} `json:"PlaceNameRegion,omitempty"`
			PlaceNameRegionTargetID int `json:"PlaceNameRegionTargetID,omitempty"`
			PlaceNameSub interface{} `json:"PlaceNameSub,omitempty"`
			DiscoveryArrayByte int `json:"DiscoveryArrayByte,omitempty"`
			MapFilenameId string `json:"MapFilenameId,omitempty"`
			OffsetX int `json:"OffsetX,omitempty"`
			PlaceName interface{} `json:"PlaceName,omitempty"`
			PlaceNameRegionTarget string `json:"PlaceNameRegionTarget,omitempty"`
			PlaceNameTarget string `json:"PlaceNameTarget,omitempty"`
			PlaceNameTargetID int `json:"PlaceNameTargetID,omitempty"`
			Hierarchy int `json:"Hierarchy,omitempty"`
			MapMarkerRange int `json:"MapMarkerRange,omitempty"`
			PlaceNameSubTargetID int `json:"PlaceNameSubTargetID,omitempty"`
			SizeFactor int `json:"SizeFactor,omitempty"`
			TerritoryTypeTarget string `json:"TerritoryTypeTarget,omitempty"`
			PlaceNameSubTarget string `json:"PlaceNameSubTarget,omitempty"`
			TerritoryType interface{} `json:"TerritoryType,omitempty"`
			TerritoryTypeTargetID int `json:"TerritoryTypeTargetID,omitempty"`
		}
		Name string `json:"Name,omitempty"`
		PlaceNameTarget string `json:"PlaceNameTarget,omitempty"`
		PlaceNameZone struct {
			NameNoArticle_de string `json:"NameNoArticle_de,omitempty"`
			NameNoArticle_ja string `json:"NameNoArticle_ja,omitempty"`
			Name_de string `json:"Name_de,omitempty"`
			Name_ja string `json:"Name_ja,omitempty"`
			ID int `json:"ID,omitempty"`
			Icon string `json:"Icon,omitempty"`
			Name string `json:"Name,omitempty"`
			NameNoArticle string `json:"NameNoArticle,omitempty"`
			NameNoArticle_en string `json:"NameNoArticle_en,omitempty"`
			NameNoArticle_fr string `json:"NameNoArticle_fr,omitempty"`
			Name_en string `json:"Name_en,omitempty"`
			Name_fr string `json:"Name_fr,omitempty"`
		}
		AetheryteTarget string `json:"AetheryteTarget,omitempty"`
		ArrayEventHandler struct {
			Data0 int `json:"Data0,omitempty"`
			Data8 int `json:"Data8,omitempty"`
			Data15 int `json:"Data15,omitempty"`
			Data6 int `json:"Data6,omitempty"`
			Data1 int `json:"Data1,omitempty"`
			Data10 int `json:"Data10,omitempty"`
			Data12 int `json:"Data12,omitempty"`
			Data5 int `json:"Data5,omitempty"`
			Data7 int `json:"Data7,omitempty"`
			ID int `json:"ID,omitempty"`
			Data13 int `json:"Data13,omitempty"`
			Data3 int `json:"Data3,omitempty"`
			Data4 int `json:"Data4,omitempty"`
			Data9 int `json:"Data9,omitempty"`
			Data11 int `json:"Data11,omitempty"`
			Data14 int `json:"Data14,omitempty"`
			Data2 int `json:"Data2,omitempty"`
		}
		Bg_en string `json:"Bg_en,omitempty"`
	}
	TerritoryTypeTargetID int `json:"TerritoryTypeTargetID,omitempty"`
	GatheringLevel int `json:"GatheringLevel,omitempty"`
	PlaceName struct {
		ID int `json:"ID,omitempty"`
		Name string `json:"Name,omitempty"`
		NameNoArticle_de string `json:"NameNoArticle_de,omitempty"`
		NameNoArticle_en string `json:"NameNoArticle_en,omitempty"`
		NameNoArticle_fr string `json:"NameNoArticle_fr,omitempty"`
		NameNoArticle_ja string `json:"NameNoArticle_ja,omitempty"`
		Name_ja string `json:"Name_ja,omitempty"`
		Icon string `json:"Icon,omitempty"`
		NameNoArticle string `json:"NameNoArticle,omitempty"`
		Name_de string `json:"Name_de,omitempty"`
		Name_en string `json:"Name_en,omitempty"`
		Name_fr string `json:"Name_fr,omitempty"`
	}
	PlaceNameTarget string `json:"PlaceNameTarget,omitempty"`
	X int `json:"X,omitempty"`
	GatheringPointBase struct {
		Item3 int `json:"Item3,omitempty"`
		Item4 int `json:"Item4,omitempty"`
		Item7 int `json:"Item7,omitempty"`
		GatheringType struct {
			IconOff string `json:"IconOff,omitempty"`
			IconOffID int `json:"IconOffID,omitempty"`
			Name_fr string `json:"Name_fr,omitempty"`
			Name_ja string `json:"Name_ja,omitempty"`
			IconMainID int `json:"IconMainID,omitempty"`
			IconMain string `json:"IconMain,omitempty"`
			Name string `json:"Name,omitempty"`
			Name_de string `json:"Name_de,omitempty"`
			Name_en string `json:"Name_en,omitempty"`
			ID int `json:"ID,omitempty"`
		}
		IsLimited int `json:"IsLimited,omitempty"`
		GatheringLevel int `json:"GatheringLevel,omitempty"`
		Item0 int `json:"Item0,omitempty"`
		ID int `json:"ID,omitempty"`
		Item2 int `json:"Item2,omitempty"`
		GatheringTypeTarget string `json:"GatheringTypeTarget,omitempty"`
		GatheringTypeTargetID int `json:"GatheringTypeTargetID,omitempty"`
		Item6 int `json:"Item6,omitempty"`
		Item1 int `json:"Item1,omitempty"`
		Item5 int `json:"Item5,omitempty"`
	}
	GatheringPointBaseTargetID int `json:"GatheringPointBaseTargetID,omitempty"`
	TerritoryTypeTarget string `json:"TerritoryTypeTarget,omitempty"`
	Url string `json:"Url,omitempty"`
	Y int `json:"Y,omitempty"`
}
