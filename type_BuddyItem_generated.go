// Code generated by generate_structs - DO NOT EDIT.

package xivapi

type BuddyItem struct {
	GameContentLinks []interface{} `json:"GameContentLinks,omitempty"`
	ID int `json:"ID,omitempty"`
	Item struct {
		BlockRate int `json:"BlockRate,omitempty"`
		DamagePhys int `json:"DamagePhys,omitempty"`
		GrandCompany interface{} `json:"GrandCompany,omitempty"`
		IsDyeable int `json:"IsDyeable,omitempty"`
		Singular_de string `json:"Singular_de,omitempty"`
		Singular_en string `json:"Singular_en,omitempty"`
		BaseParamSpecial4TargetID int `json:"BaseParamSpecial4TargetID,omitempty"`
		Description_de string `json:"Description_de,omitempty"`
		Description_fr string `json:"Description_fr,omitempty"`
		ItemSpecialBonusTargetID int `json:"ItemSpecialBonusTargetID,omitempty"`
		BaseParamValueSpecial5 int `json:"BaseParamValueSpecial5,omitempty"`
		BaseParamSpecial4 interface{} `json:"BaseParamSpecial4,omitempty"`
		ClassJobUseTarget string `json:"ClassJobUseTarget,omitempty"`
		Name_de string `json:"Name_de,omitempty"`
		Plural_de string `json:"Plural_de,omitempty"`
		BaseParamSpecial0 interface{} `json:"BaseParamSpecial0,omitempty"`
		LevelEquip int `json:"LevelEquip,omitempty"`
		ItemActionTarget string `json:"ItemActionTarget,omitempty"`
		ItemGlamourTargetID int `json:"ItemGlamourTargetID,omitempty"`
		ItemSearchCategoryTargetID int `json:"ItemSearchCategoryTargetID,omitempty"`
		Plural_fr string `json:"Plural_fr,omitempty"`
		BaseParam3Target string `json:"BaseParam3Target,omitempty"`
		ItemSpecialBonusTarget string `json:"ItemSpecialBonusTarget,omitempty"`
		Plural string `json:"Plural,omitempty"`
		SalvageTarget string `json:"SalvageTarget,omitempty"`
		Description_en string `json:"Description_en,omitempty"`
		LevelItem int `json:"LevelItem,omitempty"`
		MateriaSlotCount int `json:"MateriaSlotCount,omitempty"`
		DefenseMag int `json:"DefenseMag,omitempty"`
		BaseParam1Target string `json:"BaseParam1Target,omitempty"`
		BaseParam5 interface{} `json:"BaseParam5,omitempty"`
		BaseParamSpecial1 interface{} `json:"BaseParamSpecial1,omitempty"`
		Description_ja string `json:"Description_ja,omitempty"`
		IsPvP int `json:"IsPvP,omitempty"`
		ItemRepair interface{} `json:"ItemRepair,omitempty"`
		ItemUICategoryTargetID int `json:"ItemUICategoryTargetID,omitempty"`
		BaseParam0 interface{} `json:"BaseParam0,omitempty"`
		CooldownS int `json:"CooldownS,omitempty"`
		ItemUICategory struct {
			Name string `json:"Name,omitempty"`
			Name_en string `json:"Name_en,omitempty"`
			Name_fr string `json:"Name_fr,omitempty"`
			OrderMinor int `json:"OrderMinor,omitempty"`
			ID int `json:"ID,omitempty"`
			Icon string `json:"Icon,omitempty"`
			Name_ja string `json:"Name_ja,omitempty"`
			OrderMajor int `json:"OrderMajor,omitempty"`
			IconID int `json:"IconID,omitempty"`
			Name_de string `json:"Name_de,omitempty"`
		}
		Name_ja string `json:"Name_ja,omitempty"`
		BaseParam2 interface{} `json:"BaseParam2,omitempty"`
		BaseParam1 interface{} `json:"BaseParam1,omitempty"`
		BaseParamSpecial2TargetID int `json:"BaseParamSpecial2TargetID,omitempty"`
		BaseParamSpecial4Target string `json:"BaseParamSpecial4Target,omitempty"`
		BaseParamValue1 int `json:"BaseParamValue1,omitempty"`
		BaseParamValue2 int `json:"BaseParamValue2,omitempty"`
		ClassJobRepairTarget string `json:"ClassJobRepairTarget,omitempty"`
		Plural_en string `json:"Plural_en,omitempty"`
		AetherialReduce int `json:"AetherialReduce,omitempty"`
		ModelMain string `json:"ModelMain,omitempty"`
		IsAdvancedMeldingPermitted int `json:"IsAdvancedMeldingPermitted,omitempty"`
		IsUnique int `json:"IsUnique,omitempty"`
		GrandCompanyTargetID int `json:"GrandCompanyTargetID,omitempty"`
		BaseParam3TargetID int `json:"BaseParam3TargetID,omitempty"`
		BaseParamValueSpecial4 int `json:"BaseParamValueSpecial4,omitempty"`
		DamageMag int `json:"DamageMag,omitempty"`
		IsCrestWorthy int `json:"IsCrestWorthy,omitempty"`
		ItemSearchCategoryTarget string `json:"ItemSearchCategoryTarget,omitempty"`
		Singular_ja string `json:"Singular_ja,omitempty"`
		BaseParam0TargetID int `json:"BaseParam0TargetID,omitempty"`
		BaseParamValueSpecial2 int `json:"BaseParamValueSpecial2,omitempty"`
		ClassJobUse interface{} `json:"ClassJobUse,omitempty"`
		EquipSlotCategoryTarget string `json:"EquipSlotCategoryTarget,omitempty"`
		ItemRepairTarget string `json:"ItemRepairTarget,omitempty"`
		Plural_ja string `json:"Plural_ja,omitempty"`
		SalvageTargetID int `json:"SalvageTargetID,omitempty"`
		BaseParamSpecial5 interface{} `json:"BaseParamSpecial5,omitempty"`
		BaseParam4 interface{} `json:"BaseParam4,omitempty"`
		BaseParam5TargetID int `json:"BaseParam5TargetID,omitempty"`
		BaseParamValue3 int `json:"BaseParamValue3,omitempty"`
		BaseParam0Target string `json:"BaseParam0Target,omitempty"`
		FilterGroup int `json:"FilterGroup,omitempty"`
		ItemSeriesTargetID int `json:"ItemSeriesTargetID,omitempty"`
		ItemSpecialBonusParam int `json:"ItemSpecialBonusParam,omitempty"`
		BaseParamSpecial0TargetID int `json:"BaseParamSpecial0TargetID,omitempty"`
		BaseParam5Target string `json:"BaseParam5Target,omitempty"`
		Block int `json:"Block,omitempty"`
		Salvage interface{} `json:"Salvage,omitempty"`
		BaseParam2TargetID int `json:"BaseParam2TargetID,omitempty"`
		BaseParamSpecial2 interface{} `json:"BaseParamSpecial2,omitempty"`
		ClassJobRepair interface{} `json:"ClassJobRepair,omitempty"`
		IsGlamourous int `json:"IsGlamourous,omitempty"`
		ItemRepairTargetID int `json:"ItemRepairTargetID,omitempty"`
		ItemSeries interface{} `json:"ItemSeries,omitempty"`
		Rarity int `json:"Rarity,omitempty"`
		Singular_fr string `json:"Singular_fr,omitempty"`
		BaseParamSpecial1Target string `json:"BaseParamSpecial1Target,omitempty"`
		BaseParamValueSpecial3 int `json:"BaseParamValueSpecial3,omitempty"`
		ClassJobCategoryTargetID int `json:"ClassJobCategoryTargetID,omitempty"`
		IsCollectable int `json:"IsCollectable,omitempty"`
		PriceLow int `json:"PriceLow,omitempty"`
		BaseParamSpecial5TargetID int `json:"BaseParamSpecial5TargetID,omitempty"`
		BaseParamSpecial5Target string `json:"BaseParamSpecial5Target,omitempty"`
		DefensePhys int `json:"DefensePhys,omitempty"`
		IsUntradable int `json:"IsUntradable,omitempty"`
		ItemSeriesTarget string `json:"ItemSeriesTarget,omitempty"`
		StackSize int `json:"StackSize,omitempty"`
		BaseParamSpecial3 interface{} `json:"BaseParamSpecial3,omitempty"`
		Description string `json:"Description,omitempty"`
		Icon string `json:"Icon,omitempty"`
		BaseParamSpecial0Target string `json:"BaseParamSpecial0Target,omitempty"`
		BaseParamSpecial3Target string `json:"BaseParamSpecial3Target,omitempty"`
		ClassJobCategory interface{} `json:"ClassJobCategory,omitempty"`
		ID int `json:"ID,omitempty"`
		ItemGlamourTarget string `json:"ItemGlamourTarget,omitempty"`
		ItemSpecialBonus interface{} `json:"ItemSpecialBonus,omitempty"`
		BaseParam4Target string `json:"BaseParam4Target,omitempty"`
		ItemAction interface{} `json:"ItemAction,omitempty"`
		ItemActionTargetID int `json:"ItemActionTargetID,omitempty"`
		ItemSearchCategory struct {
			IconID int `json:"IconID,omitempty"`
			Name_de string `json:"Name_de,omitempty"`
			Name_en string `json:"Name_en,omitempty"`
			Name_fr string `json:"Name_fr,omitempty"`
			Name_ja string `json:"Name_ja,omitempty"`
			Category int `json:"Category,omitempty"`
			ClassJob interface{} `json:"ClassJob,omitempty"`
			ClassJobTarget string `json:"ClassJobTarget,omitempty"`
			ClassJobTargetID int `json:"ClassJobTargetID,omitempty"`
			ID int `json:"ID,omitempty"`
			Icon string `json:"Icon,omitempty"`
			Name string `json:"Name,omitempty"`
			Order int `json:"Order,omitempty"`
		}
		ModelSub string `json:"ModelSub,omitempty"`
		BaseParamValue0 int `json:"BaseParamValue0,omitempty"`
		BaseParamValueSpecial1 int `json:"BaseParamValueSpecial1,omitempty"`
		IconID int `json:"IconID,omitempty"`
		ItemGlamour interface{} `json:"ItemGlamour,omitempty"`
		BaseParamSpecial2Target string `json:"BaseParamSpecial2Target,omitempty"`
		BaseParamSpecial1TargetID int `json:"BaseParamSpecial1TargetID,omitempty"`
		Singular string `json:"Singular,omitempty"`
		BaseParam3 interface{} `json:"BaseParam3,omitempty"`
		BaseParam2Target string `json:"BaseParam2Target,omitempty"`
		BaseParamModifier int `json:"BaseParamModifier,omitempty"`
		ClassJobRepairTargetID int `json:"ClassJobRepairTargetID,omitempty"`
		ClassJobUseTargetID int `json:"ClassJobUseTargetID,omitempty"`
		DelayMs int `json:"DelayMs,omitempty"`
		Name string `json:"Name,omitempty"`
		Name_fr string `json:"Name_fr,omitempty"`
		BaseParam1TargetID int `json:"BaseParam1TargetID,omitempty"`
		EquipRestriction int `json:"EquipRestriction,omitempty"`
		EquipSlotCategory interface{} `json:"EquipSlotCategory,omitempty"`
		BaseParamValue4 int `json:"BaseParamValue4,omitempty"`
		BaseParamValueSpecial0 int `json:"BaseParamValueSpecial0,omitempty"`
		BaseParamSpecial3TargetID int `json:"BaseParamSpecial3TargetID,omitempty"`
		CanBeHq int `json:"CanBeHq,omitempty"`
		ClassJobCategoryTarget string `json:"ClassJobCategoryTarget,omitempty"`
		EquipSlotCategoryTargetID int `json:"EquipSlotCategoryTargetID,omitempty"`
		IsEquippable int `json:"IsEquippable,omitempty"`
		IsIndisposable int `json:"IsIndisposable,omitempty"`
		ItemUICategoryTarget string `json:"ItemUICategoryTarget,omitempty"`
		Name_en string `json:"Name_en,omitempty"`
		AdditionalData int `json:"AdditionalData,omitempty"`
		PriceMid int `json:"PriceMid,omitempty"`
		BaseParamValue5 int `json:"BaseParamValue5,omitempty"`
		GrandCompanyTarget string `json:"GrandCompanyTarget,omitempty"`
		MaterializeType int `json:"MaterializeType,omitempty"`
		StartsWithVowel int `json:"StartsWithVowel,omitempty"`
		BaseParam4TargetID int `json:"BaseParam4TargetID,omitempty"`
	}
	ItemTarget string `json:"ItemTarget,omitempty"`
	ItemTargetID int `json:"ItemTargetID,omitempty"`
	Url string `json:"Url,omitempty"`
}
