// Code generated by generate_structs - DO NOT EDIT.

package xivapi

type DeepDungeonItem struct {
	Tooltip_fr string `json:"Tooltip_fr,omitempty"`
	ActionTargetID int `json:"ActionTargetID,omitempty"`
	Rarity int `json:"Rarity,omitempty"`
	Tooltip string `json:"Tooltip,omitempty"`
	ActionTarget string `json:"ActionTarget,omitempty"`
	Singular_de string `json:"Singular_de,omitempty"`
	Tooltip_en string `json:"Tooltip_en,omitempty"`
	Name string `json:"Name,omitempty"`
	Name_en string `json:"Name_en,omitempty"`
	Name_fr string `json:"Name_fr,omitempty"`
	Singular string `json:"Singular,omitempty"`
	Singular_fr string `json:"Singular_fr,omitempty"`
	Icon string `json:"Icon,omitempty"`
	IconID int `json:"IconID,omitempty"`
	Plural string `json:"Plural,omitempty"`
	Singular_ja string `json:"Singular_ja,omitempty"`
	StartsWithVowel int `json:"StartsWithVowel,omitempty"`
	Tooltip_ja string `json:"Tooltip_ja,omitempty"`
	Action struct {
		AnimationStart interface{} `json:"AnimationStart,omitempty"`
		ClassJob interface{} `json:"ClassJob,omitempty"`
		ClassJobCategoryTarget string `json:"ClassJobCategoryTarget,omitempty"`
		ClassJobTargetID int `json:"ClassJobTargetID,omitempty"`
		Omen interface{} `json:"Omen,omitempty"`
		ActionCategoryTargetID int `json:"ActionCategoryTargetID,omitempty"`
		CanTargetDead int `json:"CanTargetDead,omitempty"`
		Name_en string `json:"Name_en,omitempty"`
		PreservesCombo int `json:"PreservesCombo,omitempty"`
		CanTargetSelf int `json:"CanTargetSelf,omitempty"`
		OmenTargetID int `json:"OmenTargetID,omitempty"`
		VFXTargetID int `json:"VFXTargetID,omitempty"`
		AnimationEndTargetID int `json:"AnimationEndTargetID,omitempty"`
		CooldownGroup int `json:"CooldownGroup,omitempty"`
		AffectsPosition int `json:"AffectsPosition,omitempty"`
		Aspect int `json:"Aspect,omitempty"`
		CanTargetFriendly int `json:"CanTargetFriendly,omitempty"`
		IconID int `json:"IconID,omitempty"`
		XAxisModifier int `json:"XAxisModifier,omitempty"`
		AnimationStartTarget string `json:"AnimationStartTarget,omitempty"`
		CostType int `json:"CostType,omitempty"`
		UnlockLink int `json:"UnlockLink,omitempty"`
		OmenTarget string `json:"OmenTarget,omitempty"`
		ActionCategory interface{} `json:"ActionCategory,omitempty"`
		AnimationEndTarget string `json:"AnimationEndTarget,omitempty"`
		AnimationStartTargetID int `json:"AnimationStartTargetID,omitempty"`
		AttackTypeTarget string `json:"AttackTypeTarget,omitempty"`
		CanTargetHostile int `json:"CanTargetHostile,omitempty"`
		IsPvP int `json:"IsPvP,omitempty"`
		IsRoleAction int `json:"IsRoleAction,omitempty"`
		ActionProcStatus interface{} `json:"ActionProcStatus,omitempty"`
		ActionTimelineHitTarget string `json:"ActionTimelineHitTarget,omitempty"`
		AttackType interface{} `json:"AttackType,omitempty"`
		CanTargetParty int `json:"CanTargetParty,omitempty"`
		Cost int `json:"Cost,omitempty"`
		ActionTimelineHit struct {
			Key string `json:"Key,omitempty"`
			Key_en string `json:"Key_en,omitempty"`
			ID int `json:"ID,omitempty"`
		}
		AnimationEnd struct {
			ID int `json:"ID,omitempty"`
			Key string `json:"Key,omitempty"`
			Key_en string `json:"Key_en,omitempty"`
		}
		ClassJobTarget string `json:"ClassJobTarget,omitempty"`
		Name_de string `json:"Name_de,omitempty"`
		Cast100ms int `json:"Cast100ms,omitempty"`
		CastType int `json:"CastType,omitempty"`
		StatusGainSelf interface{} `json:"StatusGainSelf,omitempty"`
		StatusGainSelfTarget string `json:"StatusGainSelfTarget,omitempty"`
		ActionProcStatusTarget string `json:"ActionProcStatusTarget,omitempty"`
		ClassJobCategoryTargetID int `json:"ClassJobCategoryTargetID,omitempty"`
		Icon string `json:"Icon,omitempty"`
		Name_ja string `json:"Name_ja,omitempty"`
		VFX interface{} `json:"VFX,omitempty"`
		TargetArea int `json:"TargetArea,omitempty"`
		ActionComboTargetID int `json:"ActionComboTargetID,omitempty"`
		ActionTimelineHitTargetID int `json:"ActionTimelineHitTargetID,omitempty"`
		AttackTypeTargetID int `json:"AttackTypeTargetID,omitempty"`
		ClassJobCategory interface{} `json:"ClassJobCategory,omitempty"`
		ID int `json:"ID,omitempty"`
		Name string `json:"Name,omitempty"`
		Range int `json:"Range,omitempty"`
		ActionProcStatusTargetID int `json:"ActionProcStatusTargetID,omitempty"`
		VFXTarget string `json:"VFXTarget,omitempty"`
		ActionCategoryTarget string `json:"ActionCategoryTarget,omitempty"`
		ActionCombo interface{} `json:"ActionCombo,omitempty"`
		ActionComboTarget string `json:"ActionComboTarget,omitempty"`
		EffectRange int `json:"EffectRange,omitempty"`
		StatusGainSelfTargetID int `json:"StatusGainSelfTargetID,omitempty"`
		ClassJobLevel int `json:"ClassJobLevel,omitempty"`
		Name_fr string `json:"Name_fr,omitempty"`
		Recast100ms int `json:"Recast100ms,omitempty"`
	}
	Plural_en string `json:"Plural_en,omitempty"`
	Plural_fr string `json:"Plural_fr,omitempty"`
	Plural_ja string `json:"Plural_ja,omitempty"`
	Name_ja string `json:"Name_ja,omitempty"`
	Plural_de string `json:"Plural_de,omitempty"`
	Url string `json:"Url,omitempty"`
	Singular_en string `json:"Singular_en,omitempty"`
	Tooltip_de string `json:"Tooltip_de,omitempty"`
	GameContentLinks []interface{} `json:"GameContentLinks,omitempty"`
	ID int `json:"ID,omitempty"`
	Name_de string `json:"Name_de,omitempty"`
}
