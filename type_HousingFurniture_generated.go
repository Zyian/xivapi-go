// Code generated by generate_structs - DO NOT EDIT.

package xivapi

type HousingFurniture struct {
	HousingItemCategory interface{} `json:"HousingItemCategory,omitempty"`
	Tooltip interface{} `json:"Tooltip,omitempty"`
	UsageType int `json:"UsageType,omitempty"`
	CustomTalk struct {
		ScriptInstruction22_ja string `json:"ScriptInstruction22_ja,omitempty"`
		ScriptInstruction23 string `json:"ScriptInstruction23,omitempty"`
		ScriptInstruction8_de string `json:"ScriptInstruction8_de,omitempty"`
		Name_fr string `json:"Name_fr,omitempty"`
		ScriptArg19 int `json:"ScriptArg19,omitempty"`
		ScriptInstruction15_de string `json:"ScriptInstruction15_de,omitempty"`
		ScriptInstruction21_en string `json:"ScriptInstruction21_en,omitempty"`
		ScriptInstruction22 string `json:"ScriptInstruction22,omitempty"`
		ScriptInstruction14_de string `json:"ScriptInstruction14_de,omitempty"`
		ScriptInstruction19_de string `json:"ScriptInstruction19_de,omitempty"`
		ScriptInstruction26_de string `json:"ScriptInstruction26_de,omitempty"`
		ScriptInstruction8 string `json:"ScriptInstruction8,omitempty"`
		ScriptInstruction4_en string `json:"ScriptInstruction4_en,omitempty"`
		ScriptInstruction9_en string `json:"ScriptInstruction9_en,omitempty"`
		ScriptInstruction0_fr string `json:"ScriptInstruction0_fr,omitempty"`
		ScriptInstruction10_en string `json:"ScriptInstruction10_en,omitempty"`
		ScriptInstruction25 string `json:"ScriptInstruction25,omitempty"`
		ScriptInstruction27_ja string `json:"ScriptInstruction27_ja,omitempty"`
		ScriptInstruction28_de string `json:"ScriptInstruction28_de,omitempty"`
		IconActorID int `json:"IconActorID,omitempty"`
		ScriptArg12 int `json:"ScriptArg12,omitempty"`
		ScriptInstruction25_fr string `json:"ScriptInstruction25_fr,omitempty"`
		ScriptInstruction9_fr string `json:"ScriptInstruction9_fr,omitempty"`
		ScriptArg4 int `json:"ScriptArg4,omitempty"`
		ScriptInstruction10_ja string `json:"ScriptInstruction10_ja,omitempty"`
		ScriptInstruction1_fr string `json:"ScriptInstruction1_fr,omitempty"`
		ScriptInstruction2_en string `json:"ScriptInstruction2_en,omitempty"`
		ScriptInstruction5 string `json:"ScriptInstruction5,omitempty"`
		ScriptInstruction27 string `json:"ScriptInstruction27,omitempty"`
		ScriptInstruction29 string `json:"ScriptInstruction29,omitempty"`
		ScriptInstruction29_fr string `json:"ScriptInstruction29_fr,omitempty"`
		IconMapID int `json:"IconMapID,omitempty"`
		ScriptInstruction12_fr string `json:"ScriptInstruction12_fr,omitempty"`
		ScriptInstruction14_en string `json:"ScriptInstruction14_en,omitempty"`
		ScriptInstruction1_de string `json:"ScriptInstruction1_de,omitempty"`
		ScriptInstruction26_en string `json:"ScriptInstruction26_en,omitempty"`
		ScriptInstruction8_fr string `json:"ScriptInstruction8_fr,omitempty"`
		ScriptInstruction19_ja string `json:"ScriptInstruction19_ja,omitempty"`
		ScriptInstruction21_de string `json:"ScriptInstruction21_de,omitempty"`
		ScriptInstruction23_ja string `json:"ScriptInstruction23_ja,omitempty"`
		ScriptInstruction28_fr string `json:"ScriptInstruction28_fr,omitempty"`
		ScriptInstruction29_en string `json:"ScriptInstruction29_en,omitempty"`
		ScriptInstruction12_ja string `json:"ScriptInstruction12_ja,omitempty"`
		ScriptInstruction19_fr string `json:"ScriptInstruction19_fr,omitempty"`
		ScriptInstruction24 string `json:"ScriptInstruction24,omitempty"`
		ScriptInstruction26 string `json:"ScriptInstruction26,omitempty"`
		ScriptInstruction7 string `json:"ScriptInstruction7,omitempty"`
		ScriptInstruction25_en string `json:"ScriptInstruction25_en,omitempty"`
		ScriptInstruction29_ja string `json:"ScriptInstruction29_ja,omitempty"`
		ScriptInstruction2_fr string `json:"ScriptInstruction2_fr,omitempty"`
		ScriptArg22 int `json:"ScriptArg22,omitempty"`
		ScriptInstruction12_de string `json:"ScriptInstruction12_de,omitempty"`
		ScriptInstruction14 string `json:"ScriptInstruction14,omitempty"`
		ScriptInstruction16_fr string `json:"ScriptInstruction16_fr,omitempty"`
		ScriptInstruction19 string `json:"ScriptInstruction19,omitempty"`
		ScriptInstruction5_en string `json:"ScriptInstruction5_en,omitempty"`
		ScriptInstruction7_en string `json:"ScriptInstruction7_en,omitempty"`
		ScriptInstruction2 string `json:"ScriptInstruction2,omitempty"`
		ScriptInstruction20 string `json:"ScriptInstruction20,omitempty"`
		ScriptInstruction3 string `json:"ScriptInstruction3,omitempty"`
		ScriptInstruction5_fr string `json:"ScriptInstruction5_fr,omitempty"`
		ScriptInstruction6_fr string `json:"ScriptInstruction6_fr,omitempty"`
		ScriptArg17 int `json:"ScriptArg17,omitempty"`
		ScriptArg5 int `json:"ScriptArg5,omitempty"`
		ScriptInstruction16_en string `json:"ScriptInstruction16_en,omitempty"`
		ScriptInstruction17 string `json:"ScriptInstruction17,omitempty"`
		ScriptInstruction4 string `json:"ScriptInstruction4,omitempty"`
		ScriptInstruction0_de string `json:"ScriptInstruction0_de,omitempty"`
		ScriptInstruction11 string `json:"ScriptInstruction11,omitempty"`
		ScriptInstruction15_en string `json:"ScriptInstruction15_en,omitempty"`
		ScriptInstruction29_de string `json:"ScriptInstruction29_de,omitempty"`
		ScriptInstruction18_en string `json:"ScriptInstruction18_en,omitempty"`
		ScriptInstruction24_de string `json:"ScriptInstruction24_de,omitempty"`
		ScriptInstruction28_en string `json:"ScriptInstruction28_en,omitempty"`
		ScriptArg0 int `json:"ScriptArg0,omitempty"`
		ScriptInstruction0_en string `json:"ScriptInstruction0_en,omitempty"`
		ScriptInstruction11_ja string `json:"ScriptInstruction11_ja,omitempty"`
		ScriptInstruction13_en string `json:"ScriptInstruction13_en,omitempty"`
		ScriptInstruction16 string `json:"ScriptInstruction16,omitempty"`
		ScriptInstruction7_de string `json:"ScriptInstruction7_de,omitempty"`
		ScriptArg24 int `json:"ScriptArg24,omitempty"`
		ScriptInstruction11_fr string `json:"ScriptInstruction11_fr,omitempty"`
		ScriptInstruction17_fr string `json:"ScriptInstruction17_fr,omitempty"`
		ScriptInstruction24_fr string `json:"ScriptInstruction24_fr,omitempty"`
		ScriptInstruction3_de string `json:"ScriptInstruction3_de,omitempty"`
		ScriptInstruction26_ja string `json:"ScriptInstruction26_ja,omitempty"`
		ScriptInstruction2_de string `json:"ScriptInstruction2_de,omitempty"`
		ScriptInstruction4_de string `json:"ScriptInstruction4_de,omitempty"`
		Name_en string `json:"Name_en,omitempty"`
		ScriptArg11 int `json:"ScriptArg11,omitempty"`
		ScriptArg15 int `json:"ScriptArg15,omitempty"`
		ScriptInstruction17_de string `json:"ScriptInstruction17_de,omitempty"`
		ScriptInstruction22_en string `json:"ScriptInstruction22_en,omitempty"`
		ScriptInstruction7_ja string `json:"ScriptInstruction7_ja,omitempty"`
		ScriptInstruction8_ja string `json:"ScriptInstruction8_ja,omitempty"`
		ScriptInstruction4_fr string `json:"ScriptInstruction4_fr,omitempty"`
		ScriptInstruction0_ja string `json:"ScriptInstruction0_ja,omitempty"`
		ScriptInstruction10_de string `json:"ScriptInstruction10_de,omitempty"`
		ScriptInstruction18_de string `json:"ScriptInstruction18_de,omitempty"`
		ScriptInstruction21 string `json:"ScriptInstruction21,omitempty"`
		ScriptInstruction3_ja string `json:"ScriptInstruction3_ja,omitempty"`
		ScriptArg25 int `json:"ScriptArg25,omitempty"`
		ScriptInstruction21_fr string `json:"ScriptInstruction21_fr,omitempty"`
		ScriptInstruction25_ja string `json:"ScriptInstruction25_ja,omitempty"`
		ScriptInstruction15 string `json:"ScriptInstruction15,omitempty"`
		ScriptInstruction15_fr string `json:"ScriptInstruction15_fr,omitempty"`
		ScriptInstruction15_ja string `json:"ScriptInstruction15_ja,omitempty"`
		ID int `json:"ID,omitempty"`
		ScriptArg23 int `json:"ScriptArg23,omitempty"`
		ScriptArg28 int `json:"ScriptArg28,omitempty"`
		ScriptArg29 int `json:"ScriptArg29,omitempty"`
		ScriptInstruction13_fr string `json:"ScriptInstruction13_fr,omitempty"`
		ScriptInstruction23_en string `json:"ScriptInstruction23_en,omitempty"`
		ScriptInstruction26_fr string `json:"ScriptInstruction26_fr,omitempty"`
		ScriptInstruction27_de string `json:"ScriptInstruction27_de,omitempty"`
		ScriptInstruction28 string `json:"ScriptInstruction28,omitempty"`
		ScriptInstruction14_ja string `json:"ScriptInstruction14_ja,omitempty"`
		ScriptInstruction20_fr string `json:"ScriptInstruction20_fr,omitempty"`
		ScriptInstruction27_fr string `json:"ScriptInstruction27_fr,omitempty"`
		ScriptArg13 int `json:"ScriptArg13,omitempty"`
		ScriptInstruction10 string `json:"ScriptInstruction10,omitempty"`
		ScriptInstruction17_en string `json:"ScriptInstruction17_en,omitempty"`
		ScriptInstruction20_en string `json:"ScriptInstruction20_en,omitempty"`
		ScriptInstruction22_fr string `json:"ScriptInstruction22_fr,omitempty"`
		Name string `json:"Name,omitempty"`
		ScriptInstruction1_en string `json:"ScriptInstruction1_en,omitempty"`
		ScriptInstruction3_fr string `json:"ScriptInstruction3_fr,omitempty"`
		ScriptInstruction5_ja string `json:"ScriptInstruction5_ja,omitempty"`
		IconMap string `json:"IconMap,omitempty"`
		ScriptArg10 int `json:"ScriptArg10,omitempty"`
		ScriptArg26 int `json:"ScriptArg26,omitempty"`
		ScriptInstruction17_ja string `json:"ScriptInstruction17_ja,omitempty"`
		ScriptInstruction27_en string `json:"ScriptInstruction27_en,omitempty"`
		ScriptInstruction24_en string `json:"ScriptInstruction24_en,omitempty"`
		ScriptInstruction25_de string `json:"ScriptInstruction25_de,omitempty"`
		ScriptInstruction28_ja string `json:"ScriptInstruction28_ja,omitempty"`
		Name_de string `json:"Name_de,omitempty"`
		ScriptArg16 int `json:"ScriptArg16,omitempty"`
		ScriptInstruction19_en string `json:"ScriptInstruction19_en,omitempty"`
		ScriptInstruction20_ja string `json:"ScriptInstruction20_ja,omitempty"`
		ScriptInstruction23_fr string `json:"ScriptInstruction23_fr,omitempty"`
		ScriptInstruction5_de string `json:"ScriptInstruction5_de,omitempty"`
		ScriptInstruction6_en string `json:"ScriptInstruction6_en,omitempty"`
		ScriptInstruction13_de string `json:"ScriptInstruction13_de,omitempty"`
		ScriptInstruction18_fr string `json:"ScriptInstruction18_fr,omitempty"`
		ScriptInstruction18_ja string `json:"ScriptInstruction18_ja,omitempty"`
		ScriptArg3 int `json:"ScriptArg3,omitempty"`
		ScriptInstruction11_de string `json:"ScriptInstruction11_de,omitempty"`
		ScriptInstruction11_en string `json:"ScriptInstruction11_en,omitempty"`
		ScriptInstruction12 string `json:"ScriptInstruction12,omitempty"`
		ScriptInstruction12_en string `json:"ScriptInstruction12_en,omitempty"`
		ScriptInstruction8_en string `json:"ScriptInstruction8_en,omitempty"`
		ScriptInstruction9_de string `json:"ScriptInstruction9_de,omitempty"`
		ScriptInstruction16_ja string `json:"ScriptInstruction16_ja,omitempty"`
		ScriptInstruction20_de string `json:"ScriptInstruction20_de,omitempty"`
		ScriptInstruction23_de string `json:"ScriptInstruction23_de,omitempty"`
		ScriptArg21 int `json:"ScriptArg21,omitempty"`
		ScriptArg27 int `json:"ScriptArg27,omitempty"`
		ScriptArg8 int `json:"ScriptArg8,omitempty"`
		ScriptArg9 int `json:"ScriptArg9,omitempty"`
		ScriptInstruction10_fr string `json:"ScriptInstruction10_fr,omitempty"`
		ScriptInstruction24_ja string `json:"ScriptInstruction24_ja,omitempty"`
		ScriptInstruction3_en string `json:"ScriptInstruction3_en,omitempty"`
		ScriptInstruction6_de string `json:"ScriptInstruction6_de,omitempty"`
		ScriptInstruction9_ja string `json:"ScriptInstruction9_ja,omitempty"`
		ScriptArg18 int `json:"ScriptArg18,omitempty"`
		ScriptInstruction14_fr string `json:"ScriptInstruction14_fr,omitempty"`
		ScriptInstruction16_de string `json:"ScriptInstruction16_de,omitempty"`
		ScriptInstruction22_de string `json:"ScriptInstruction22_de,omitempty"`
		ScriptInstruction6 string `json:"ScriptInstruction6,omitempty"`
		ScriptInstruction6_ja string `json:"ScriptInstruction6_ja,omitempty"`
		ScriptInstruction21_ja string `json:"ScriptInstruction21_ja,omitempty"`
		ScriptInstruction2_ja string `json:"ScriptInstruction2_ja,omitempty"`
		ScriptArg6 int `json:"ScriptArg6,omitempty"`
		ScriptArg7 int `json:"ScriptArg7,omitempty"`
		ScriptInstruction0 string `json:"ScriptInstruction0,omitempty"`
		ScriptInstruction1 string `json:"ScriptInstruction1,omitempty"`
		ScriptInstruction1_ja string `json:"ScriptInstruction1_ja,omitempty"`
		ScriptInstruction7_fr string `json:"ScriptInstruction7_fr,omitempty"`
		Text int `json:"Text,omitempty"`
		Name_ja string `json:"Name_ja,omitempty"`
		ScriptArg14 int `json:"ScriptArg14,omitempty"`
		ScriptArg20 int `json:"ScriptArg20,omitempty"`
		ScriptInstruction13 string `json:"ScriptInstruction13,omitempty"`
		ScriptInstruction13_ja string `json:"ScriptInstruction13_ja,omitempty"`
		IconActor string `json:"IconActor,omitempty"`
		ScriptArg2 int `json:"ScriptArg2,omitempty"`
		ScriptInstruction9 string `json:"ScriptInstruction9,omitempty"`
		ScriptArg1 int `json:"ScriptArg1,omitempty"`
		ScriptInstruction18 string `json:"ScriptInstruction18,omitempty"`
		ScriptInstruction4_ja string `json:"ScriptInstruction4_ja,omitempty"`
	}
	CustomTalkTarget string `json:"CustomTalkTarget,omitempty"`
	CustomTalkTargetID int `json:"CustomTalkTargetID,omitempty"`
	GameContentLinks []interface{} `json:"GameContentLinks,omitempty"`
	HousingItemCategoryTargetID int `json:"HousingItemCategoryTargetID,omitempty"`
	TooltipTarget string `json:"TooltipTarget,omitempty"`
	TooltipTargetID int `json:"TooltipTargetID,omitempty"`
	HousingItemCategoryTarget string `json:"HousingItemCategoryTarget,omitempty"`
	HousingLayoutLimit interface{} `json:"HousingLayoutLimit,omitempty"`
	Item struct {
		ItemGlamour interface{} `json:"ItemGlamour,omitempty"`
		ModelMain string `json:"ModelMain,omitempty"`
		Name_de string `json:"Name_de,omitempty"`
		Plural_en string `json:"Plural_en,omitempty"`
		BaseParam0Target string `json:"BaseParam0Target,omitempty"`
		EquipSlotCategoryTarget string `json:"EquipSlotCategoryTarget,omitempty"`
		BaseParam4Target string `json:"BaseParam4Target,omitempty"`
		ClassJobRepair struct {
			ItemSoulCrystalTarget string `json:"ItemSoulCrystalTarget,omitempty"`
			ModifierPiety int `json:"ModifierPiety,omitempty"`
			ModifierManaPoints int `json:"ModifierManaPoints,omitempty"`
			Icon string `json:"Icon,omitempty"`
			ItemStartingWeaponTarget string `json:"ItemStartingWeaponTarget,omitempty"`
			LimitBreak2 interface{} `json:"LimitBreak2,omitempty"`
			LimitBreak3 interface{} `json:"LimitBreak3,omitempty"`
			Abbreviation_en string `json:"Abbreviation_en,omitempty"`
			LimitBreak2Target string `json:"LimitBreak2Target,omitempty"`
			LimitBreak3Target string `json:"LimitBreak3Target,omitempty"`
			Name_ja string `json:"Name_ja,omitempty"`
			UnlockQuestTargetID int `json:"UnlockQuestTargetID,omitempty"`
			ClassJobParentTarget string `json:"ClassJobParentTarget,omitempty"`
			ItemStartingWeapon interface{} `json:"ItemStartingWeapon,omitempty"`
			Name_en string `json:"Name_en,omitempty"`
			ModifierIntelligence int `json:"ModifierIntelligence,omitempty"`
			Abbreviation string `json:"Abbreviation,omitempty"`
			Abbreviation_fr string `json:"Abbreviation_fr,omitempty"`
			ClassJobParent interface{} `json:"ClassJobParent,omitempty"`
			ItemSoulCrystal interface{} `json:"ItemSoulCrystal,omitempty"`
			LimitBreak3TargetID int `json:"LimitBreak3TargetID,omitempty"`
			ModifierDexterity int `json:"ModifierDexterity,omitempty"`
			ClassJobCategory interface{} `json:"ClassJobCategory,omitempty"`
			ClassJobParentTargetID int `json:"ClassJobParentTargetID,omitempty"`
			ID int `json:"ID,omitempty"`
			NameEnglish_en string `json:"NameEnglish_en,omitempty"`
			NameEnglish_fr string `json:"NameEnglish_fr,omitempty"`
			StartingLevel int `json:"StartingLevel,omitempty"`
			ExpArrayIndex int `json:"ExpArrayIndex,omitempty"`
			LimitBreak1TargetID int `json:"LimitBreak1TargetID,omitempty"`
			LimitBreak2TargetID int `json:"LimitBreak2TargetID,omitempty"`
			Abbreviation_ja string `json:"Abbreviation_ja,omitempty"`
			Name_de string `json:"Name_de,omitempty"`
			ModifierStrength int `json:"ModifierStrength,omitempty"`
			ModifierVitality int `json:"ModifierVitality,omitempty"`
			NameEnglish_de string `json:"NameEnglish_de,omitempty"`
			ClassJobCategoryTargetID int `json:"ClassJobCategoryTargetID,omitempty"`
			Name string `json:"Name,omitempty"`
			PrerequisiteTargetID int `json:"PrerequisiteTargetID,omitempty"`
			RelicQuestTargetID int `json:"RelicQuestTargetID,omitempty"`
			ItemStartingWeaponTargetID int `json:"ItemStartingWeaponTargetID,omitempty"`
			ModifierHitPoints int `json:"ModifierHitPoints,omitempty"`
			Prerequisite interface{} `json:"Prerequisite,omitempty"`
			PrerequisiteTarget string `json:"PrerequisiteTarget,omitempty"`
			ClassJobCategoryTarget string `json:"ClassJobCategoryTarget,omitempty"`
			LimitBreak1Target string `json:"LimitBreak1Target,omitempty"`
			ModifierMind int `json:"ModifierMind,omitempty"`
			RelicQuest interface{} `json:"RelicQuest,omitempty"`
			LimitBreak1 interface{} `json:"LimitBreak1,omitempty"`
			NameEnglish string `json:"NameEnglish,omitempty"`
			ItemSoulCrystalTargetID int `json:"ItemSoulCrystalTargetID,omitempty"`
			Name_fr string `json:"Name_fr,omitempty"`
			UnlockQuestTarget string `json:"UnlockQuestTarget,omitempty"`
			Abbreviation_de string `json:"Abbreviation_de,omitempty"`
			NameEnglish_ja string `json:"NameEnglish_ja,omitempty"`
			RelicQuestTarget string `json:"RelicQuestTarget,omitempty"`
			UnlockQuest interface{} `json:"UnlockQuest,omitempty"`
		}
		DelayMs int `json:"DelayMs,omitempty"`
		Description_ja string `json:"Description_ja,omitempty"`
		IsCollectable int `json:"IsCollectable,omitempty"`
		LevelItem int `json:"LevelItem,omitempty"`
		AetherialReduce int `json:"AetherialReduce,omitempty"`
		BaseParam1Target string `json:"BaseParam1Target,omitempty"`
		PriceMid int `json:"PriceMid,omitempty"`
		EquipSlotCategory interface{} `json:"EquipSlotCategory,omitempty"`
		BaseParam3Target string `json:"BaseParam3Target,omitempty"`
		BaseParamValue4 int `json:"BaseParamValue4,omitempty"`
		ItemSeriesTarget string `json:"ItemSeriesTarget,omitempty"`
		Singular_ja string `json:"Singular_ja,omitempty"`
		BaseParam3 interface{} `json:"BaseParam3,omitempty"`
		BaseParamSpecial3 interface{} `json:"BaseParamSpecial3,omitempty"`
		CooldownS int `json:"CooldownS,omitempty"`
		ItemSpecialBonus interface{} `json:"ItemSpecialBonus,omitempty"`
		BaseParam5TargetID int `json:"BaseParam5TargetID,omitempty"`
		DamagePhys int `json:"DamagePhys,omitempty"`
		IconID int `json:"IconID,omitempty"`
		ItemRepairTargetID int `json:"ItemRepairTargetID,omitempty"`
		ItemSearchCategory struct {
			ClassJobTargetID int `json:"ClassJobTargetID,omitempty"`
			IconID int `json:"IconID,omitempty"`
			Name string `json:"Name,omitempty"`
			Name_de string `json:"Name_de,omitempty"`
			Name_fr string `json:"Name_fr,omitempty"`
			Order int `json:"Order,omitempty"`
			Category int `json:"Category,omitempty"`
			ClassJob interface{} `json:"ClassJob,omitempty"`
			ClassJobTarget string `json:"ClassJobTarget,omitempty"`
			ID int `json:"ID,omitempty"`
			Icon string `json:"Icon,omitempty"`
			Name_en string `json:"Name_en,omitempty"`
			Name_ja string `json:"Name_ja,omitempty"`
		}
		Plural_fr string `json:"Plural_fr,omitempty"`
		BaseParam1 interface{} `json:"BaseParam1,omitempty"`
		DamageMag int `json:"DamageMag,omitempty"`
		BaseParamValue5 int `json:"BaseParamValue5,omitempty"`
		DefensePhys int `json:"DefensePhys,omitempty"`
		BaseParamSpecial3TargetID int `json:"BaseParamSpecial3TargetID,omitempty"`
		BlockRate int `json:"BlockRate,omitempty"`
		Block int `json:"Block,omitempty"`
		BaseParamValueSpecial3 int `json:"BaseParamValueSpecial3,omitempty"`
		ItemSpecialBonusTargetID int `json:"ItemSpecialBonusTargetID,omitempty"`
		LevelEquip int `json:"LevelEquip,omitempty"`
		ModelSub string `json:"ModelSub,omitempty"`
		BaseParam3TargetID int `json:"BaseParam3TargetID,omitempty"`
		BaseParamSpecial5TargetID int `json:"BaseParamSpecial5TargetID,omitempty"`
		BaseParamSpecial5 interface{} `json:"BaseParamSpecial5,omitempty"`
		IsDyeable int `json:"IsDyeable,omitempty"`
		ItemRepairTarget string `json:"ItemRepairTarget,omitempty"`
		Salvage struct {
			ID int `json:"ID,omitempty"`
			OptimalSkill int `json:"OptimalSkill,omitempty"`
		}
		Singular_fr string `json:"Singular_fr,omitempty"`
		BaseParam2 interface{} `json:"BaseParam2,omitempty"`
		BaseParamModifier int `json:"BaseParamModifier,omitempty"`
		SalvageTargetID int `json:"SalvageTargetID,omitempty"`
		Icon string `json:"Icon,omitempty"`
		PriceLow int `json:"PriceLow,omitempty"`
		Description_fr string `json:"Description_fr,omitempty"`
		ItemActionTargetID int `json:"ItemActionTargetID,omitempty"`
		BaseParam0TargetID int `json:"BaseParam0TargetID,omitempty"`
		BaseParamSpecial0Target string `json:"BaseParamSpecial0Target,omitempty"`
		ClassJobRepairTarget string `json:"ClassJobRepairTarget,omitempty"`
		IsGlamourous int `json:"IsGlamourous,omitempty"`
		ItemGlamourTarget string `json:"ItemGlamourTarget,omitempty"`
		StackSize int `json:"StackSize,omitempty"`
		AdditionalData int `json:"AdditionalData,omitempty"`
		ClassJobCategory interface{} `json:"ClassJobCategory,omitempty"`
		Name string `json:"Name,omitempty"`
		Name_en string `json:"Name_en,omitempty"`
		Rarity int `json:"Rarity,omitempty"`
		BaseParamSpecial4 interface{} `json:"BaseParamSpecial4,omitempty"`
		ItemUICategory struct {
			Name string `json:"Name,omitempty"`
			Name_fr string `json:"Name_fr,omitempty"`
			OrderMinor int `json:"OrderMinor,omitempty"`
			ID int `json:"ID,omitempty"`
			Icon string `json:"Icon,omitempty"`
			IconID int `json:"IconID,omitempty"`
			Name_de string `json:"Name_de,omitempty"`
			Name_en string `json:"Name_en,omitempty"`
			Name_ja string `json:"Name_ja,omitempty"`
			OrderMajor int `json:"OrderMajor,omitempty"`
		}
		EquipRestriction int `json:"EquipRestriction,omitempty"`
		IsIndisposable int `json:"IsIndisposable,omitempty"`
		ItemActionTarget string `json:"ItemActionTarget,omitempty"`
		Name_ja string `json:"Name_ja,omitempty"`
		BaseParam2Target string `json:"BaseParam2Target,omitempty"`
		BaseParamSpecial1Target string `json:"BaseParamSpecial1Target,omitempty"`
		BaseParam2TargetID int `json:"BaseParam2TargetID,omitempty"`
		IsAdvancedMeldingPermitted int `json:"IsAdvancedMeldingPermitted,omitempty"`
		ClassJobUse interface{} `json:"ClassJobUse,omitempty"`
		IsEquippable int `json:"IsEquippable,omitempty"`
		ItemSeries interface{} `json:"ItemSeries,omitempty"`
		BaseParamSpecial1TargetID int `json:"BaseParamSpecial1TargetID,omitempty"`
		ClassJobRepairTargetID int `json:"ClassJobRepairTargetID,omitempty"`
		BaseParamValueSpecial4 int `json:"BaseParamValueSpecial4,omitempty"`
		MaterializeType int `json:"MaterializeType,omitempty"`
		Singular string `json:"Singular,omitempty"`
		BaseParam0 interface{} `json:"BaseParam0,omitempty"`
		BaseParam5 interface{} `json:"BaseParam5,omitempty"`
		BaseParamSpecial5Target string `json:"BaseParamSpecial5Target,omitempty"`
		BaseParamValue1 int `json:"BaseParamValue1,omitempty"`
		BaseParamValue3 int `json:"BaseParamValue3,omitempty"`
		BaseParamValueSpecial2 int `json:"BaseParamValueSpecial2,omitempty"`
		GrandCompanyTargetID int `json:"GrandCompanyTargetID,omitempty"`
		ItemSpecialBonusParam int `json:"ItemSpecialBonusParam,omitempty"`
		BaseParam5Target string `json:"BaseParam5Target,omitempty"`
		BaseParamSpecial4Target string `json:"BaseParamSpecial4Target,omitempty"`
		BaseParamValueSpecial0 int `json:"BaseParamValueSpecial0,omitempty"`
		BaseParamValueSpecial5 int `json:"BaseParamValueSpecial5,omitempty"`
		ItemGlamourTargetID int `json:"ItemGlamourTargetID,omitempty"`
		Description_en string `json:"Description_en,omitempty"`
		ItemAction interface{} `json:"ItemAction,omitempty"`
		DefenseMag int `json:"DefenseMag,omitempty"`
		ItemSearchCategoryTarget string `json:"ItemSearchCategoryTarget,omitempty"`
		ItemSpecialBonusTarget string `json:"ItemSpecialBonusTarget,omitempty"`
		Name_fr string `json:"Name_fr,omitempty"`
		BaseParamSpecial0TargetID int `json:"BaseParamSpecial0TargetID,omitempty"`
		BaseParamValue2 int `json:"BaseParamValue2,omitempty"`
		ClassJobUseTargetID int `json:"ClassJobUseTargetID,omitempty"`
		Description_de string `json:"Description_de,omitempty"`
		EquipSlotCategoryTargetID int `json:"EquipSlotCategoryTargetID,omitempty"`
		Plural string `json:"Plural,omitempty"`
		BaseParamSpecial4TargetID int `json:"BaseParamSpecial4TargetID,omitempty"`
		ClassJobCategoryTarget string `json:"ClassJobCategoryTarget,omitempty"`
		ClassJobCategoryTargetID int `json:"ClassJobCategoryTargetID,omitempty"`
		IsPvP int `json:"IsPvP,omitempty"`
		BaseParamValue0 int `json:"BaseParamValue0,omitempty"`
		BaseParamValueSpecial1 int `json:"BaseParamValueSpecial1,omitempty"`
		GrandCompanyTarget string `json:"GrandCompanyTarget,omitempty"`
		IsUntradable int `json:"IsUntradable,omitempty"`
		BaseParam4 interface{} `json:"BaseParam4,omitempty"`
		BaseParam4TargetID int `json:"BaseParam4TargetID,omitempty"`
		IsUnique int `json:"IsUnique,omitempty"`
		BaseParamSpecial1 interface{} `json:"BaseParamSpecial1,omitempty"`
		BaseParamSpecial2TargetID int `json:"BaseParamSpecial2TargetID,omitempty"`
		CanBeHq int `json:"CanBeHq,omitempty"`
		FilterGroup int `json:"FilterGroup,omitempty"`
		ItemSearchCategoryTargetID int `json:"ItemSearchCategoryTargetID,omitempty"`
		ItemSeriesTargetID int `json:"ItemSeriesTargetID,omitempty"`
		ItemUICategoryTargetID int `json:"ItemUICategoryTargetID,omitempty"`
		SalvageTarget string `json:"SalvageTarget,omitempty"`
		BaseParamSpecial0 interface{} `json:"BaseParamSpecial0,omitempty"`
		BaseParamSpecial2 interface{} `json:"BaseParamSpecial2,omitempty"`
		Description string `json:"Description,omitempty"`
		ID int `json:"ID,omitempty"`
		BaseParamSpecial3Target string `json:"BaseParamSpecial3Target,omitempty"`
		Plural_de string `json:"Plural_de,omitempty"`
		Plural_ja string `json:"Plural_ja,omitempty"`
		StartsWithVowel int `json:"StartsWithVowel,omitempty"`
		BaseParam1TargetID int `json:"BaseParam1TargetID,omitempty"`
		BaseParamSpecial2Target string `json:"BaseParamSpecial2Target,omitempty"`
		IsCrestWorthy int `json:"IsCrestWorthy,omitempty"`
		ItemRepair interface{} `json:"ItemRepair,omitempty"`
		ItemUICategoryTarget string `json:"ItemUICategoryTarget,omitempty"`
		MateriaSlotCount int `json:"MateriaSlotCount,omitempty"`
		Singular_de string `json:"Singular_de,omitempty"`
		Singular_en string `json:"Singular_en,omitempty"`
		ClassJobUseTarget string `json:"ClassJobUseTarget,omitempty"`
		GrandCompany interface{} `json:"GrandCompany,omitempty"`
	}
	ItemTargetID int `json:"ItemTargetID,omitempty"`
	ModelKey int `json:"ModelKey,omitempty"`
	Url string `json:"Url,omitempty"`
	DestroyOnRemoval int `json:"DestroyOnRemoval,omitempty"`
	HousingLayoutLimitTarget string `json:"HousingLayoutLimitTarget,omitempty"`
	HousingLayoutLimitTargetID int `json:"HousingLayoutLimitTargetID,omitempty"`
	ID int `json:"ID,omitempty"`
	ItemTarget string `json:"ItemTarget,omitempty"`
	UsageParameter int `json:"UsageParameter,omitempty"`
}
