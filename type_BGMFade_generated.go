// Code generated by generate_structs - DO NOT EDIT.

package xivapi

type BGMFade struct {
	BGMFadeType struct {
		ID int `json:"ID,omitempty"`
	}
	BGMFadeTypeTarget string `json:"BGMFadeTypeTarget,omitempty"`
	BGMFadeTypeTargetID int `json:"BGMFadeTypeTargetID,omitempty"`
	GameContentLinks []interface{} `json:"GameContentLinks,omitempty"`
	ID int `json:"ID,omitempty"`
	Url string `json:"Url,omitempty"`
}
