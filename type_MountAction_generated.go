// Code generated by generate_structs - DO NOT EDIT.

package xivapi

type MountAction struct {
	Action1TargetID int `json:"Action1TargetID,omitempty"`
	Action2Target string `json:"Action2Target,omitempty"`
	Action2TargetID int `json:"Action2TargetID,omitempty"`
	Action3TargetID int `json:"Action3TargetID,omitempty"`
	Url string `json:"Url,omitempty"`
	Action0 struct {
		EffectRange int `json:"EffectRange,omitempty"`
		Icon string `json:"Icon,omitempty"`
		Name_ja string `json:"Name_ja,omitempty"`
		ClassJobCategory struct {
			Name_de string `json:"Name_de,omitempty"`
			PGL int `json:"PGL,omitempty"`
			WAR int `json:"WAR,omitempty"`
			MCH int `json:"MCH,omitempty"`
			MRD int `json:"MRD,omitempty"`
			Name_en string `json:"Name_en,omitempty"`
			SCH int `json:"SCH,omitempty"`
			BLM int `json:"BLM,omitempty"`
			FSH int `json:"FSH,omitempty"`
			GSM int `json:"GSM,omitempty"`
			LNC int `json:"LNC,omitempty"`
			THM int `json:"THM,omitempty"`
			ADV int `json:"ADV,omitempty"`
			PLD int `json:"PLD,omitempty"`
			WVR int `json:"WVR,omitempty"`
			SMN int `json:"SMN,omitempty"`
			AST int `json:"AST,omitempty"`
			BSM int `json:"BSM,omitempty"`
			DRK int `json:"DRK,omitempty"`
			ROG int `json:"ROG,omitempty"`
			ARC int `json:"ARC,omitempty"`
			GLA int `json:"GLA,omitempty"`
			Name string `json:"Name,omitempty"`
			SAM int `json:"SAM,omitempty"`
			ARM int `json:"ARM,omitempty"`
			CUL int `json:"CUL,omitempty"`
			DRG int `json:"DRG,omitempty"`
			RDM int `json:"RDM,omitempty"`
			BRD int `json:"BRD,omitempty"`
			ID int `json:"ID,omitempty"`
			Name_fr string `json:"Name_fr,omitempty"`
			CRP int `json:"CRP,omitempty"`
			LTW int `json:"LTW,omitempty"`
			MIN int `json:"MIN,omitempty"`
			MNK int `json:"MNK,omitempty"`
			ACN int `json:"ACN,omitempty"`
			ALC int `json:"ALC,omitempty"`
			BTN int `json:"BTN,omitempty"`
			CNJ int `json:"CNJ,omitempty"`
			NIN int `json:"NIN,omitempty"`
			Name_ja string `json:"Name_ja,omitempty"`
			WHM int `json:"WHM,omitempty"`
		}
		XAxisModifier int `json:"XAxisModifier,omitempty"`
		ActionTimelineHit struct {
			ID int `json:"ID,omitempty"`
			Key string `json:"Key,omitempty"`
			Key_en string `json:"Key_en,omitempty"`
		}
		AnimationEndTargetID int `json:"AnimationEndTargetID,omitempty"`
		TargetArea int `json:"TargetArea,omitempty"`
		UnlockLink int `json:"UnlockLink,omitempty"`
		Range int `json:"Range,omitempty"`
		ActionCategoryTargetID int `json:"ActionCategoryTargetID,omitempty"`
		AffectsPosition int `json:"AffectsPosition,omitempty"`
		ClassJobCategoryTargetID int `json:"ClassJobCategoryTargetID,omitempty"`
		IconID int `json:"IconID,omitempty"`
		Name_de string `json:"Name_de,omitempty"`
		OmenTarget string `json:"OmenTarget,omitempty"`
		VFXTarget string `json:"VFXTarget,omitempty"`
		ActionCombo interface{} `json:"ActionCombo,omitempty"`
		ActionProcStatusTarget string `json:"ActionProcStatusTarget,omitempty"`
		CanTargetDead int `json:"CanTargetDead,omitempty"`
		Name string `json:"Name,omitempty"`
		ActionProcStatusTargetID int `json:"ActionProcStatusTargetID,omitempty"`
		ClassJobCategoryTarget string `json:"ClassJobCategoryTarget,omitempty"`
		ClassJobLevel int `json:"ClassJobLevel,omitempty"`
		IsRoleAction int `json:"IsRoleAction,omitempty"`
		StatusGainSelfTarget string `json:"StatusGainSelfTarget,omitempty"`
		AttackTypeTargetID int `json:"AttackTypeTargetID,omitempty"`
		Cast100ms int `json:"Cast100ms,omitempty"`
		CastType int `json:"CastType,omitempty"`
		Name_fr string `json:"Name_fr,omitempty"`
		ActionProcStatus interface{} `json:"ActionProcStatus,omitempty"`
		ActionTimelineHitTarget string `json:"ActionTimelineHitTarget,omitempty"`
		AnimationStart interface{} `json:"AnimationStart,omitempty"`
		Aspect int `json:"Aspect,omitempty"`
		AnimationStartTargetID int `json:"AnimationStartTargetID,omitempty"`
		AttackTypeTarget string `json:"AttackTypeTarget,omitempty"`
		OmenTargetID int `json:"OmenTargetID,omitempty"`
		StatusGainSelf interface{} `json:"StatusGainSelf,omitempty"`
		VFX interface{} `json:"VFX,omitempty"`
		AttackType interface{} `json:"AttackType,omitempty"`
		CanTargetFriendly int `json:"CanTargetFriendly,omitempty"`
		CanTargetSelf int `json:"CanTargetSelf,omitempty"`
		ClassJob interface{} `json:"ClassJob,omitempty"`
		ActionComboTargetID int `json:"ActionComboTargetID,omitempty"`
		CooldownGroup int `json:"CooldownGroup,omitempty"`
		Recast100ms int `json:"Recast100ms,omitempty"`
		StatusGainSelfTargetID int `json:"StatusGainSelfTargetID,omitempty"`
		ClassJobTarget string `json:"ClassJobTarget,omitempty"`
		ClassJobTargetID int `json:"ClassJobTargetID,omitempty"`
		ActionCategory struct {
			Name_en string `json:"Name_en,omitempty"`
			Name_fr string `json:"Name_fr,omitempty"`
			Name_ja string `json:"Name_ja,omitempty"`
			ID int `json:"ID,omitempty"`
			Name string `json:"Name,omitempty"`
			Name_de string `json:"Name_de,omitempty"`
		}
		AnimationStartTarget string `json:"AnimationStartTarget,omitempty"`
		CostType int `json:"CostType,omitempty"`
		IsPvP int `json:"IsPvP,omitempty"`
		AnimationEnd struct {
			ID int `json:"ID,omitempty"`
			Key string `json:"Key,omitempty"`
			Key_en string `json:"Key_en,omitempty"`
		}
		Cost int `json:"Cost,omitempty"`
		Name_en string `json:"Name_en,omitempty"`
		VFXTargetID int `json:"VFXTargetID,omitempty"`
		CanTargetHostile int `json:"CanTargetHostile,omitempty"`
		CanTargetParty int `json:"CanTargetParty,omitempty"`
		ID int `json:"ID,omitempty"`
		Omen interface{} `json:"Omen,omitempty"`
		ActionCategoryTarget string `json:"ActionCategoryTarget,omitempty"`
		ActionComboTarget string `json:"ActionComboTarget,omitempty"`
		ActionTimelineHitTargetID int `json:"ActionTimelineHitTargetID,omitempty"`
		AnimationEndTarget string `json:"AnimationEndTarget,omitempty"`
		PreservesCombo int `json:"PreservesCombo,omitempty"`
	}
	Action0TargetID int `json:"Action0TargetID,omitempty"`
	Action1Target string `json:"Action1Target,omitempty"`
	Action2 interface{} `json:"Action2,omitempty"`
	Action4 interface{} `json:"Action4,omitempty"`
	ID int `json:"ID,omitempty"`
	Action5TargetID int `json:"Action5TargetID,omitempty"`
	Action0Target string `json:"Action0Target,omitempty"`
	Action1 interface{} `json:"Action1,omitempty"`
	Action3Target string `json:"Action3Target,omitempty"`
	Action4Target string `json:"Action4Target,omitempty"`
	Action4TargetID int `json:"Action4TargetID,omitempty"`
	Action5 interface{} `json:"Action5,omitempty"`
	Action5Target string `json:"Action5Target,omitempty"`
	Action3 interface{} `json:"Action3,omitempty"`
	GameContentLinks []interface{} `json:"GameContentLinks,omitempty"`
}
