// Code generated by generate_structs - DO NOT EDIT.

package xivapi

type AirshipExplorationLevel struct {
	GameContentLinks []interface{} `json:"GameContentLinks,omitempty"`
	ID int `json:"ID,omitempty"`
	Url string `json:"Url,omitempty"`
	ExpToNext int `json:"ExpToNext,omitempty"`
}
