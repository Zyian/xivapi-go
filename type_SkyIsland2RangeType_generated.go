// Code generated by generate_structs - DO NOT EDIT.

package xivapi

type SkyIsland2RangeType struct {
	Type int `json:"Type,omitempty"`
	Url string `json:"Url,omitempty"`
	GameContentLinks struct {
		SkyIsland2MissionDetail struct {
			Range []interface{} `json:"Range,omitempty"`
		}
	}
	ID int `json:"ID,omitempty"`
}
