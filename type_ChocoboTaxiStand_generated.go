// Code generated by generate_structs - DO NOT EDIT.

package xivapi

type ChocoboTaxiStand struct {
	PlaceName_de string `json:"PlaceName_de,omitempty"`
	PlaceName_en string `json:"PlaceName_en,omitempty"`
	PlaceName_fr string `json:"PlaceName_fr,omitempty"`
	PlaceName_ja string `json:"PlaceName_ja,omitempty"`
	Url string `json:"Url,omitempty"`
	GameContentLinks struct {
		ChocoboTaxi struct {
			Location []interface{} `json:"Location,omitempty"`
		}
	}
	ID int `json:"ID,omitempty"`
	PlaceName string `json:"PlaceName,omitempty"`
}
