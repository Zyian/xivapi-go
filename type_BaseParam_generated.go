// Code generated by generate_structs - DO NOT EDIT.

package xivapi

type BaseParam struct {
	ChestLegsGlovesPerc int `json:"ChestLegsGloves%,omitempty"`
	Description_ja string `json:"Description_ja,omitempty"`
	BraceletPerc int `json:"Bracelet%,omitempty"`
	ChestHeadPerc int `json:"ChestHead%,omitempty"`
	LegsPerc int `json:"Legs%,omitempty"`
	NecklacePerc int `json:"Necklace%,omitempty"`
	HeadPerc int `json:"Head%,omitempty"`
	ID int `json:"ID,omitempty"`
	OHPerc int `json:"OH%,omitempty"`
	EarringPerc int `json:"Earring%,omitempty"`
	FeetPerc int `json:"Feet%,omitempty"`
	GameContentLinks struct {
		ItemFood struct {
			BaseParam0 []interface{} `json:"BaseParam0,omitempty"`
		}
		Materia struct {
			BaseParam []interface{} `json:"BaseParam,omitempty"`
		}
	}
	HandsPerc int `json:"Hands%,omitempty"`
	Name_ja string `json:"Name_ja,omitempty"`
	WaistPerc int `json:"Waist%,omitempty"`
	T2HWpnPerc int `json:"2HWpn%,omitempty"`
	Description_fr string `json:"Description_fr,omitempty"`
	Name_fr string `json:"Name_fr,omitempty"`
	T1HWpnPerc int `json:"1HWpn%,omitempty"`
	Name_en string `json:"Name_en,omitempty"`
	Url string `json:"Url,omitempty"`
	Name_de string `json:"Name_de,omitempty"`
	RingPerc int `json:"Ring%,omitempty"`
	ChestLegsFeetPerc int `json:"ChestLegsFeet%,omitempty"`
	HeadChestHandsLegsFeetPerc int `json:"HeadChestHandsLegsFeet%,omitempty"`
	LegsFeetPerc int `json:"LegsFeet%,omitempty"`
	Name string `json:"Name,omitempty"`
	Description_en string `json:"Description_en,omitempty"`
	ChestPerc int `json:"Chest%,omitempty"`
	ChestHeadLegsFeetPerc int `json:"ChestHeadLegsFeet%,omitempty"`
	Description string `json:"Description,omitempty"`
	Description_de string `json:"Description_de,omitempty"`
}
