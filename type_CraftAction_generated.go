// Code generated by generate_structs - DO NOT EDIT.

package xivapi

type CraftAction struct {
	ClassJobLevel int `json:"ClassJobLevel,omitempty"`
	GSM struct {
		AnimationEnd int `json:"AnimationEnd,omitempty"`
		CRP int `json:"CRP,omitempty"`
		ALC int `json:"ALC,omitempty"`
		Cost int `json:"Cost,omitempty"`
		Description_fr string `json:"Description_fr,omitempty"`
		GSM int `json:"GSM,omitempty"`
		Specialist int `json:"Specialist,omitempty"`
		ARM int `json:"ARM,omitempty"`
		ClassJobCategory int `json:"ClassJobCategory,omitempty"`
		ID int `json:"ID,omitempty"`
		LTW int `json:"LTW,omitempty"`
		Name_de string `json:"Name_de,omitempty"`
		Name_en string `json:"Name_en,omitempty"`
		WVR int `json:"WVR,omitempty"`
		CUL int `json:"CUL,omitempty"`
		Description string `json:"Description,omitempty"`
		Description_en string `json:"Description_en,omitempty"`
		IconID int `json:"IconID,omitempty"`
		Name string `json:"Name,omitempty"`
		Name_fr string `json:"Name_fr,omitempty"`
		BSM int `json:"BSM,omitempty"`
		ClassJob int `json:"ClassJob,omitempty"`
		ClassJobLevel int `json:"ClassJobLevel,omitempty"`
		Description_ja string `json:"Description_ja,omitempty"`
		Icon string `json:"Icon,omitempty"`
		Description_de string `json:"Description_de,omitempty"`
		AnimationStart int `json:"AnimationStart,omitempty"`
		Name_ja string `json:"Name_ja,omitempty"`
		QuestRequirement int `json:"QuestRequirement,omitempty"`
	}
	Specialist int `json:"Specialist,omitempty"`
	AnimationStartTarget string `json:"AnimationStartTarget,omitempty"`
	CULTargetID int `json:"CULTargetID,omitempty"`
	Description_cn interface{} `json:"Description_cn,omitempty"`
	CULTarget string `json:"CULTarget,omitempty"`
	Cost int `json:"Cost,omitempty"`
	Description_en string `json:"Description_en,omitempty"`
	Name_fr string `json:"Name_fr,omitempty"`
	WVR struct {
		Description_en string `json:"Description_en,omitempty"`
		CUL int `json:"CUL,omitempty"`
		ClassJobLevel int `json:"ClassJobLevel,omitempty"`
		Description_fr string `json:"Description_fr,omitempty"`
		Description_ja string `json:"Description_ja,omitempty"`
		Icon string `json:"Icon,omitempty"`
		ALC int `json:"ALC,omitempty"`
		ARM int `json:"ARM,omitempty"`
		Cost int `json:"Cost,omitempty"`
		ID int `json:"ID,omitempty"`
		AnimationEnd int `json:"AnimationEnd,omitempty"`
		BSM int `json:"BSM,omitempty"`
		GSM int `json:"GSM,omitempty"`
		Name string `json:"Name,omitempty"`
		Name_ja string `json:"Name_ja,omitempty"`
		WVR int `json:"WVR,omitempty"`
		LTW int `json:"LTW,omitempty"`
		QuestRequirement int `json:"QuestRequirement,omitempty"`
		AnimationStart int `json:"AnimationStart,omitempty"`
		Description_de string `json:"Description_de,omitempty"`
		ClassJob int `json:"ClassJob,omitempty"`
		ClassJobCategory int `json:"ClassJobCategory,omitempty"`
		IconID int `json:"IconID,omitempty"`
		Name_fr string `json:"Name_fr,omitempty"`
		Specialist int `json:"Specialist,omitempty"`
		CRP int `json:"CRP,omitempty"`
		Description string `json:"Description,omitempty"`
		Name_de string `json:"Name_de,omitempty"`
		Name_en string `json:"Name_en,omitempty"`
	}
	WVRTargetID int `json:"WVRTargetID,omitempty"`
	DescriptionJSON_en []interface{} `json:"DescriptionJSON_en,omitempty"`
	Icon string `json:"Icon,omitempty"`
	LTWTarget string `json:"LTWTarget,omitempty"`
	Name_de string `json:"Name_de,omitempty"`
	Name_en string `json:"Name_en,omitempty"`
	WVRTarget string `json:"WVRTarget,omitempty"`
	AnimationEndTarget string `json:"AnimationEndTarget,omitempty"`
	AnimationStartTargetID int `json:"AnimationStartTargetID,omitempty"`
	CRPTargetID int `json:"CRPTargetID,omitempty"`
	IconID int `json:"IconID,omitempty"`
	LTW struct {
		ARM int `json:"ARM,omitempty"`
		ClassJob int `json:"ClassJob,omitempty"`
		QuestRequirement int `json:"QuestRequirement,omitempty"`
		AnimationStart int `json:"AnimationStart,omitempty"`
		GSM int `json:"GSM,omitempty"`
		IconID int `json:"IconID,omitempty"`
		Name string `json:"Name,omitempty"`
		Cost int `json:"Cost,omitempty"`
		Description_de string `json:"Description_de,omitempty"`
		ID int `json:"ID,omitempty"`
		ALC int `json:"ALC,omitempty"`
		BSM int `json:"BSM,omitempty"`
		CUL int `json:"CUL,omitempty"`
		Name_fr string `json:"Name_fr,omitempty"`
		Description_en string `json:"Description_en,omitempty"`
		Description_ja string `json:"Description_ja,omitempty"`
		Specialist int `json:"Specialist,omitempty"`
		WVR int `json:"WVR,omitempty"`
		Icon string `json:"Icon,omitempty"`
		Name_de string `json:"Name_de,omitempty"`
		Name_ja string `json:"Name_ja,omitempty"`
		AnimationEnd int `json:"AnimationEnd,omitempty"`
		ClassJobCategory int `json:"ClassJobCategory,omitempty"`
		ClassJobLevel int `json:"ClassJobLevel,omitempty"`
		Description string `json:"Description,omitempty"`
		Description_fr string `json:"Description_fr,omitempty"`
		Name_en string `json:"Name_en,omitempty"`
		CRP int `json:"CRP,omitempty"`
		LTW int `json:"LTW,omitempty"`
	}
	LTWTargetID int `json:"LTWTargetID,omitempty"`
	AnimationEnd struct {
		ID int `json:"ID,omitempty"`
		Key string `json:"Key,omitempty"`
		Key_en string `json:"Key_en,omitempty"`
	}
	AnimationEndTargetID int `json:"AnimationEndTargetID,omitempty"`
	BSM struct {
		AnimationEnd int `json:"AnimationEnd,omitempty"`
		AnimationStart int `json:"AnimationStart,omitempty"`
		BSM int `json:"BSM,omitempty"`
		Icon string `json:"Icon,omitempty"`
		ARM int `json:"ARM,omitempty"`
		CRP int `json:"CRP,omitempty"`
		Name_ja string `json:"Name_ja,omitempty"`
		Specialist int `json:"Specialist,omitempty"`
		ALC int `json:"ALC,omitempty"`
		CUL int `json:"CUL,omitempty"`
		ClassJob int `json:"ClassJob,omitempty"`
		Name_en string `json:"Name_en,omitempty"`
		Description_ja string `json:"Description_ja,omitempty"`
		Name string `json:"Name,omitempty"`
		Description string `json:"Description,omitempty"`
		Description_fr string `json:"Description_fr,omitempty"`
		Name_de string `json:"Name_de,omitempty"`
		QuestRequirement int `json:"QuestRequirement,omitempty"`
		ClassJobCategory int `json:"ClassJobCategory,omitempty"`
		ClassJobLevel int `json:"ClassJobLevel,omitempty"`
		Description_de string `json:"Description_de,omitempty"`
		IconID int `json:"IconID,omitempty"`
		ID int `json:"ID,omitempty"`
		LTW int `json:"LTW,omitempty"`
		WVR int `json:"WVR,omitempty"`
		Cost int `json:"Cost,omitempty"`
		Description_en string `json:"Description_en,omitempty"`
		GSM int `json:"GSM,omitempty"`
		Name_fr string `json:"Name_fr,omitempty"`
	}
	BSMTarget string `json:"BSMTarget,omitempty"`
	ID int `json:"ID,omitempty"`
	ALCTarget string `json:"ALCTarget,omitempty"`
	ARM struct {
		CRP int `json:"CRP,omitempty"`
		Description_en string `json:"Description_en,omitempty"`
		GSM int `json:"GSM,omitempty"`
		ID int `json:"ID,omitempty"`
		Name string `json:"Name,omitempty"`
		Name_de string `json:"Name_de,omitempty"`
		Name_ja string `json:"Name_ja,omitempty"`
		BSM int `json:"BSM,omitempty"`
		CUL int `json:"CUL,omitempty"`
		Description string `json:"Description,omitempty"`
		Icon string `json:"Icon,omitempty"`
		QuestRequirement int `json:"QuestRequirement,omitempty"`
		ARM int `json:"ARM,omitempty"`
		Cost int `json:"Cost,omitempty"`
		Description_fr string `json:"Description_fr,omitempty"`
		Specialist int `json:"Specialist,omitempty"`
		ALC int `json:"ALC,omitempty"`
		AnimationEnd int `json:"AnimationEnd,omitempty"`
		AnimationStart int `json:"AnimationStart,omitempty"`
		Description_ja string `json:"Description_ja,omitempty"`
		LTW int `json:"LTW,omitempty"`
		ClassJob int `json:"ClassJob,omitempty"`
		Name_en string `json:"Name_en,omitempty"`
		Name_fr string `json:"Name_fr,omitempty"`
		ClassJobCategory int `json:"ClassJobCategory,omitempty"`
		ClassJobLevel int `json:"ClassJobLevel,omitempty"`
		IconID int `json:"IconID,omitempty"`
		Description_de string `json:"Description_de,omitempty"`
		WVR int `json:"WVR,omitempty"`
	}
	ClassJobCategoryTarget string `json:"ClassJobCategoryTarget,omitempty"`
	Description_fr string `json:"Description_fr,omitempty"`
	ALCTargetID int `json:"ALCTargetID,omitempty"`
	DescriptionJSON_ja []interface{} `json:"DescriptionJSON_ja,omitempty"`
	QuestRequirementTargetID int `json:"QuestRequirementTargetID,omitempty"`
	Name_ja string `json:"Name_ja,omitempty"`
	ARMTargetID int `json:"ARMTargetID,omitempty"`
	CUL struct {
		CUL int `json:"CUL,omitempty"`
		ClassJobCategory int `json:"ClassJobCategory,omitempty"`
		ClassJobLevel int `json:"ClassJobLevel,omitempty"`
		Description_de string `json:"Description_de,omitempty"`
		Description_en string `json:"Description_en,omitempty"`
		Icon string `json:"Icon,omitempty"`
		IconID int `json:"IconID,omitempty"`
		Name string `json:"Name,omitempty"`
		ARM int `json:"ARM,omitempty"`
		AnimationStart int `json:"AnimationStart,omitempty"`
		BSM int `json:"BSM,omitempty"`
		LTW int `json:"LTW,omitempty"`
		Name_en string `json:"Name_en,omitempty"`
		Specialist int `json:"Specialist,omitempty"`
		AnimationEnd int `json:"AnimationEnd,omitempty"`
		GSM int `json:"GSM,omitempty"`
		ID int `json:"ID,omitempty"`
		Name_de string `json:"Name_de,omitempty"`
		Name_ja string `json:"Name_ja,omitempty"`
		ALC int `json:"ALC,omitempty"`
		Name_fr string `json:"Name_fr,omitempty"`
		QuestRequirement int `json:"QuestRequirement,omitempty"`
		ClassJob int `json:"ClassJob,omitempty"`
		Cost int `json:"Cost,omitempty"`
		Description_fr string `json:"Description_fr,omitempty"`
		WVR int `json:"WVR,omitempty"`
		CRP int `json:"CRP,omitempty"`
		Description string `json:"Description,omitempty"`
		Description_ja string `json:"Description_ja,omitempty"`
	}
	ClassJobTarget string `json:"ClassJobTarget,omitempty"`
	Name string `json:"Name,omitempty"`
	ClassJobTargetID int `json:"ClassJobTargetID,omitempty"`
	DescriptionJSON []interface{} `json:"DescriptionJSON,omitempty"`
	DescriptionJSON_de []interface{} `json:"DescriptionJSON_de,omitempty"`
	Url string `json:"Url,omitempty"`
	ARMTarget string `json:"ARMTarget,omitempty"`
	CRP struct {
		LTW int `json:"LTW,omitempty"`
		Description_en string `json:"Description_en,omitempty"`
		ClassJob int `json:"ClassJob,omitempty"`
		Cost int `json:"Cost,omitempty"`
		AnimationEnd int `json:"AnimationEnd,omitempty"`
		Description_fr string `json:"Description_fr,omitempty"`
		ID int `json:"ID,omitempty"`
		Icon string `json:"Icon,omitempty"`
		Name_fr string `json:"Name_fr,omitempty"`
		ClassJobLevel int `json:"ClassJobLevel,omitempty"`
		AnimationStart int `json:"AnimationStart,omitempty"`
		QuestRequirement int `json:"QuestRequirement,omitempty"`
		ARM int `json:"ARM,omitempty"`
		IconID int `json:"IconID,omitempty"`
		Name string `json:"Name,omitempty"`
		Name_de string `json:"Name_de,omitempty"`
		Specialist int `json:"Specialist,omitempty"`
		WVR int `json:"WVR,omitempty"`
		GSM int `json:"GSM,omitempty"`
		Description string `json:"Description,omitempty"`
		Description_de string `json:"Description_de,omitempty"`
		Description_ja string `json:"Description_ja,omitempty"`
		Name_ja string `json:"Name_ja,omitempty"`
		CUL int `json:"CUL,omitempty"`
		CRP int `json:"CRP,omitempty"`
		ClassJobCategory int `json:"ClassJobCategory,omitempty"`
		Name_en string `json:"Name_en,omitempty"`
		BSM int `json:"BSM,omitempty"`
		ALC int `json:"ALC,omitempty"`
	}
	ClassJob struct {
		ClassJobParent interface{} `json:"ClassJobParent,omitempty"`
		UnlockQuest interface{} `json:"UnlockQuest,omitempty"`
		RelicQuestTargetID int `json:"RelicQuestTargetID,omitempty"`
		ModifierIntelligence int `json:"ModifierIntelligence,omitempty"`
		NameEnglish string `json:"NameEnglish,omitempty"`
		NameEnglish_en string `json:"NameEnglish_en,omitempty"`
		ModifierDexterity int `json:"ModifierDexterity,omitempty"`
		RelicQuestTarget string `json:"RelicQuestTarget,omitempty"`
		ItemStartingWeapon interface{} `json:"ItemStartingWeapon,omitempty"`
		LimitBreak2Target string `json:"LimitBreak2Target,omitempty"`
		LimitBreak2TargetID int `json:"LimitBreak2TargetID,omitempty"`
		Prerequisite interface{} `json:"Prerequisite,omitempty"`
		ItemSoulCrystal struct {
			BaseParam0Target string `json:"BaseParam0Target,omitempty"`
			BaseParamValueSpecial5 int `json:"BaseParamValueSpecial5,omitempty"`
			BlockRate int `json:"BlockRate,omitempty"`
			ItemSpecialBonusTarget string `json:"ItemSpecialBonusTarget,omitempty"`
			ItemUICategory interface{} `json:"ItemUICategory,omitempty"`
			Plural_de string `json:"Plural_de,omitempty"`
			BaseParamSpecial1 interface{} `json:"BaseParamSpecial1,omitempty"`
			IsAdvancedMeldingPermitted int `json:"IsAdvancedMeldingPermitted,omitempty"`
			IsGlamourous int `json:"IsGlamourous,omitempty"`
			ItemSpecialBonusParam int `json:"ItemSpecialBonusParam,omitempty"`
			ItemUICategoryTarget string `json:"ItemUICategoryTarget,omitempty"`
			BaseParamSpecial4Target string `json:"BaseParamSpecial4Target,omitempty"`
			Description_fr string `json:"Description_fr,omitempty"`
			FilterGroup int `json:"FilterGroup,omitempty"`
			IsEquippable int `json:"IsEquippable,omitempty"`
			Singular_en string `json:"Singular_en,omitempty"`
			BaseParam1 interface{} `json:"BaseParam1,omitempty"`
			BaseParam2TargetID int `json:"BaseParam2TargetID,omitempty"`
			CanBeHq int `json:"CanBeHq,omitempty"`
			ClassJobCategoryTarget string `json:"ClassJobCategoryTarget,omitempty"`
			LevelItem int `json:"LevelItem,omitempty"`
			SalvageTarget string `json:"SalvageTarget,omitempty"`
			StartsWithVowel int `json:"StartsWithVowel,omitempty"`
			BaseParamSpecial3Target string `json:"BaseParamSpecial3Target,omitempty"`
			BaseParamSpecial3TargetID int `json:"BaseParamSpecial3TargetID,omitempty"`
			Description_de string `json:"Description_de,omitempty"`
			EquipSlotCategoryTarget string `json:"EquipSlotCategoryTarget,omitempty"`
			ItemSpecialBonus interface{} `json:"ItemSpecialBonus,omitempty"`
			ItemUICategoryTargetID int `json:"ItemUICategoryTargetID,omitempty"`
			Singular_ja string `json:"Singular_ja,omitempty"`
			BaseParamSpecial5Target string `json:"BaseParamSpecial5Target,omitempty"`
			BaseParamValue5 int `json:"BaseParamValue5,omitempty"`
			Salvage interface{} `json:"Salvage,omitempty"`
			BaseParam0TargetID int `json:"BaseParam0TargetID,omitempty"`
			BaseParamValue0 int `json:"BaseParamValue0,omitempty"`
			GrandCompanyTarget string `json:"GrandCompanyTarget,omitempty"`
			GrandCompanyTargetID int `json:"GrandCompanyTargetID,omitempty"`
			IsPvP int `json:"IsPvP,omitempty"`
			BaseParamModifier int `json:"BaseParamModifier,omitempty"`
			DamageMag int `json:"DamageMag,omitempty"`
			IsUnique int `json:"IsUnique,omitempty"`
			ItemGlamourTargetID int `json:"ItemGlamourTargetID,omitempty"`
			ItemSearchCategoryTarget string `json:"ItemSearchCategoryTarget,omitempty"`
			Plural_fr string `json:"Plural_fr,omitempty"`
			BaseParam5 interface{} `json:"BaseParam5,omitempty"`
			Name_de string `json:"Name_de,omitempty"`
			BaseParamSpecial4 interface{} `json:"BaseParamSpecial4,omitempty"`
			BaseParamValueSpecial4 int `json:"BaseParamValueSpecial4,omitempty"`
			LevelEquip int `json:"LevelEquip,omitempty"`
			IconID int `json:"IconID,omitempty"`
			ItemSearchCategory interface{} `json:"ItemSearchCategory,omitempty"`
			BaseParam4TargetID int `json:"BaseParam4TargetID,omitempty"`
			BaseParamValue4 int `json:"BaseParamValue4,omitempty"`
			ClassJobCategory interface{} `json:"ClassJobCategory,omitempty"`
			Singular_fr string `json:"Singular_fr,omitempty"`
			BaseParam4Target string `json:"BaseParam4Target,omitempty"`
			BaseParamSpecial5 interface{} `json:"BaseParamSpecial5,omitempty"`
			BaseParamValueSpecial3 int `json:"BaseParamValueSpecial3,omitempty"`
			IsIndisposable int `json:"IsIndisposable,omitempty"`
			ModelMain string `json:"ModelMain,omitempty"`
			Singular_de string `json:"Singular_de,omitempty"`
			ClassJobRepairTarget string `json:"ClassJobRepairTarget,omitempty"`
			ItemSeriesTargetID int `json:"ItemSeriesTargetID,omitempty"`
			EquipSlotCategoryTargetID int `json:"EquipSlotCategoryTargetID,omitempty"`
			GrandCompany interface{} `json:"GrandCompany,omitempty"`
			Singular string `json:"Singular,omitempty"`
			BaseParam3Target string `json:"BaseParam3Target,omitempty"`
			DefensePhys int `json:"DefensePhys,omitempty"`
			ItemRepairTarget string `json:"ItemRepairTarget,omitempty"`
			BaseParam3TargetID int `json:"BaseParam3TargetID,omitempty"`
			ClassJobUseTarget string `json:"ClassJobUseTarget,omitempty"`
			IsUntradable int `json:"IsUntradable,omitempty"`
			BaseParam2 interface{} `json:"BaseParam2,omitempty"`
			BaseParamSpecial3 interface{} `json:"BaseParamSpecial3,omitempty"`
			BaseParamSpecial4TargetID int `json:"BaseParamSpecial4TargetID,omitempty"`
			BaseParamValue2 int `json:"BaseParamValue2,omitempty"`
			BaseParamValue3 int `json:"BaseParamValue3,omitempty"`
			ClassJobUse interface{} `json:"ClassJobUse,omitempty"`
			ClassJobUseTargetID int `json:"ClassJobUseTargetID,omitempty"`
			EquipRestriction int `json:"EquipRestriction,omitempty"`
			ItemSeries interface{} `json:"ItemSeries,omitempty"`
			MaterializeType int `json:"MaterializeType,omitempty"`
			Plural_en string `json:"Plural_en,omitempty"`
			PriceMid int `json:"PriceMid,omitempty"`
			BaseParam5TargetID int `json:"BaseParam5TargetID,omitempty"`
			ClassJobRepairTargetID int `json:"ClassJobRepairTargetID,omitempty"`
			IsDyeable int `json:"IsDyeable,omitempty"`
			SalvageTargetID int `json:"SalvageTargetID,omitempty"`
			BaseParamValueSpecial1 int `json:"BaseParamValueSpecial1,omitempty"`
			ItemSeriesTarget string `json:"ItemSeriesTarget,omitempty"`
			Plural string `json:"Plural,omitempty"`
			ItemGlamourTarget string `json:"ItemGlamourTarget,omitempty"`
			Name string `json:"Name,omitempty"`
			BaseParam1Target string `json:"BaseParam1Target,omitempty"`
			BaseParamSpecial2TargetID int `json:"BaseParamSpecial2TargetID,omitempty"`
			BaseParamSpecial5TargetID int `json:"BaseParamSpecial5TargetID,omitempty"`
			Description_en string `json:"Description_en,omitempty"`
			ItemActionTarget string `json:"ItemActionTarget,omitempty"`
			PriceLow int `json:"PriceLow,omitempty"`
			BaseParamSpecial1TargetID int `json:"BaseParamSpecial1TargetID,omitempty"`
			DefenseMag int `json:"DefenseMag,omitempty"`
			ItemGlamour interface{} `json:"ItemGlamour,omitempty"`
			ItemRepair interface{} `json:"ItemRepair,omitempty"`
			BaseParamSpecial0Target string `json:"BaseParamSpecial0Target,omitempty"`
			BaseParamValueSpecial0 int `json:"BaseParamValueSpecial0,omitempty"`
			DamagePhys int `json:"DamagePhys,omitempty"`
			MateriaSlotCount int `json:"MateriaSlotCount,omitempty"`
			Rarity int `json:"Rarity,omitempty"`
			StackSize int `json:"StackSize,omitempty"`
			BaseParam1TargetID int `json:"BaseParam1TargetID,omitempty"`
			Description string `json:"Description,omitempty"`
			Name_ja string `json:"Name_ja,omitempty"`
			BaseParam0 interface{} `json:"BaseParam0,omitempty"`
			BaseParam3 interface{} `json:"BaseParam3,omitempty"`
			BaseParam4 interface{} `json:"BaseParam4,omitempty"`
			BaseParam5Target string `json:"BaseParam5Target,omitempty"`
			Block int `json:"Block,omitempty"`
			CooldownS int `json:"CooldownS,omitempty"`
			ModelSub string `json:"ModelSub,omitempty"`
			ClassJobRepair interface{} `json:"ClassJobRepair,omitempty"`
			Icon string `json:"Icon,omitempty"`
			ItemActionTargetID int `json:"ItemActionTargetID,omitempty"`
			Name_en string `json:"Name_en,omitempty"`
			AetherialReduce int `json:"AetherialReduce,omitempty"`
			BaseParamSpecial2 interface{} `json:"BaseParamSpecial2,omitempty"`
			ClassJobCategoryTargetID int `json:"ClassJobCategoryTargetID,omitempty"`
			BaseParamSpecial0TargetID int `json:"BaseParamSpecial0TargetID,omitempty"`
			BaseParamSpecial2Target string `json:"BaseParamSpecial2Target,omitempty"`
			BaseParamValue1 int `json:"BaseParamValue1,omitempty"`
			DelayMs int `json:"DelayMs,omitempty"`
			IsCollectable int `json:"IsCollectable,omitempty"`
			IsCrestWorthy int `json:"IsCrestWorthy,omitempty"`
			ItemRepairTargetID int `json:"ItemRepairTargetID,omitempty"`
			ItemSpecialBonusTargetID int `json:"ItemSpecialBonusTargetID,omitempty"`
			BaseParamSpecial0 interface{} `json:"BaseParamSpecial0,omitempty"`
			BaseParamSpecial1Target string `json:"BaseParamSpecial1Target,omitempty"`
			BaseParamValueSpecial2 int `json:"BaseParamValueSpecial2,omitempty"`
			Description_ja string `json:"Description_ja,omitempty"`
			ItemAction interface{} `json:"ItemAction,omitempty"`
			Name_fr string `json:"Name_fr,omitempty"`
			Plural_ja string `json:"Plural_ja,omitempty"`
			AdditionalData int `json:"AdditionalData,omitempty"`
			BaseParam2Target string `json:"BaseParam2Target,omitempty"`
			EquipSlotCategory interface{} `json:"EquipSlotCategory,omitempty"`
			ID int `json:"ID,omitempty"`
			ItemSearchCategoryTargetID int `json:"ItemSearchCategoryTargetID,omitempty"`
		}
		ModifierPiety int `json:"ModifierPiety,omitempty"`
		Name_en string `json:"Name_en,omitempty"`
		ClassJobCategory struct {
			SAM int `json:"SAM,omitempty"`
			ADV int `json:"ADV,omitempty"`
			ALC int `json:"ALC,omitempty"`
			BTN int `json:"BTN,omitempty"`
			CRP int `json:"CRP,omitempty"`
			DRG int `json:"DRG,omitempty"`
			MNK int `json:"MNK,omitempty"`
			Name string `json:"Name,omitempty"`
			THM int `json:"THM,omitempty"`
			WVR int `json:"WVR,omitempty"`
			BSM int `json:"BSM,omitempty"`
			LNC int `json:"LNC,omitempty"`
			RDM int `json:"RDM,omitempty"`
			SCH int `json:"SCH,omitempty"`
			AST int `json:"AST,omitempty"`
			CUL int `json:"CUL,omitempty"`
			DRK int `json:"DRK,omitempty"`
			MCH int `json:"MCH,omitempty"`
			MRD int `json:"MRD,omitempty"`
			GSM int `json:"GSM,omitempty"`
			Name_de string `json:"Name_de,omitempty"`
			Name_ja string `json:"Name_ja,omitempty"`
			WAR int `json:"WAR,omitempty"`
			ARM int `json:"ARM,omitempty"`
			CNJ int `json:"CNJ,omitempty"`
			FSH int `json:"FSH,omitempty"`
			LTW int `json:"LTW,omitempty"`
			PGL int `json:"PGL,omitempty"`
			PLD int `json:"PLD,omitempty"`
			BLM int `json:"BLM,omitempty"`
			GLA int `json:"GLA,omitempty"`
			WHM int `json:"WHM,omitempty"`
			ARC int `json:"ARC,omitempty"`
			BRD int `json:"BRD,omitempty"`
			Name_en string `json:"Name_en,omitempty"`
			ROG int `json:"ROG,omitempty"`
			SMN int `json:"SMN,omitempty"`
			ACN int `json:"ACN,omitempty"`
			ID int `json:"ID,omitempty"`
			MIN int `json:"MIN,omitempty"`
			NIN int `json:"NIN,omitempty"`
			Name_fr string `json:"Name_fr,omitempty"`
		}
		ModifierVitality int `json:"ModifierVitality,omitempty"`
		Abbreviation_fr string `json:"Abbreviation_fr,omitempty"`
		ModifierMind int `json:"ModifierMind,omitempty"`
		Name_de string `json:"Name_de,omitempty"`
		ItemStartingWeaponTargetID int `json:"ItemStartingWeaponTargetID,omitempty"`
		LimitBreak1 interface{} `json:"LimitBreak1,omitempty"`
		LimitBreak3Target string `json:"LimitBreak3Target,omitempty"`
		ModifierStrength int `json:"ModifierStrength,omitempty"`
		Abbreviation_en string `json:"Abbreviation_en,omitempty"`
		ItemSoulCrystalTarget string `json:"ItemSoulCrystalTarget,omitempty"`
		LimitBreak1TargetID int `json:"LimitBreak1TargetID,omitempty"`
		ClassJobParentTarget string `json:"ClassJobParentTarget,omitempty"`
		Name string `json:"Name,omitempty"`
		PrerequisiteTargetID int `json:"PrerequisiteTargetID,omitempty"`
		LimitBreak3 interface{} `json:"LimitBreak3,omitempty"`
		LimitBreak3TargetID int `json:"LimitBreak3TargetID,omitempty"`
		NameEnglish_de string `json:"NameEnglish_de,omitempty"`
		NameEnglish_fr string `json:"NameEnglish_fr,omitempty"`
		RelicQuest interface{} `json:"RelicQuest,omitempty"`
		ClassJobCategoryTargetID int `json:"ClassJobCategoryTargetID,omitempty"`
		ExpArrayIndex int `json:"ExpArrayIndex,omitempty"`
		LimitBreak2 interface{} `json:"LimitBreak2,omitempty"`
		StartingLevel int `json:"StartingLevel,omitempty"`
		Name_fr string `json:"Name_fr,omitempty"`
		Name_ja string `json:"Name_ja,omitempty"`
		Abbreviation_ja string `json:"Abbreviation_ja,omitempty"`
		ClassJobCategoryTarget string `json:"ClassJobCategoryTarget,omitempty"`
		LimitBreak1Target string `json:"LimitBreak1Target,omitempty"`
		Abbreviation string `json:"Abbreviation,omitempty"`
		ModifierManaPoints int `json:"ModifierManaPoints,omitempty"`
		NameEnglish_ja string `json:"NameEnglish_ja,omitempty"`
		ModifierHitPoints int `json:"ModifierHitPoints,omitempty"`
		ID int `json:"ID,omitempty"`
		Icon string `json:"Icon,omitempty"`
		ItemSoulCrystalTargetID int `json:"ItemSoulCrystalTargetID,omitempty"`
		PrerequisiteTarget string `json:"PrerequisiteTarget,omitempty"`
		UnlockQuestTarget string `json:"UnlockQuestTarget,omitempty"`
		UnlockQuestTargetID int `json:"UnlockQuestTargetID,omitempty"`
		Abbreviation_de string `json:"Abbreviation_de,omitempty"`
		ClassJobParentTargetID int `json:"ClassJobParentTargetID,omitempty"`
		ItemStartingWeaponTarget string `json:"ItemStartingWeaponTarget,omitempty"`
	}
	ClassJobCategory struct {
		ADV int `json:"ADV,omitempty"`
		GSM int `json:"GSM,omitempty"`
		LNC int `json:"LNC,omitempty"`
		Name_fr string `json:"Name_fr,omitempty"`
		WVR int `json:"WVR,omitempty"`
		CNJ int `json:"CNJ,omitempty"`
		CRP int `json:"CRP,omitempty"`
		PLD int `json:"PLD,omitempty"`
		SAM int `json:"SAM,omitempty"`
		GLA int `json:"GLA,omitempty"`
		MRD int `json:"MRD,omitempty"`
		ROG int `json:"ROG,omitempty"`
		ARC int `json:"ARC,omitempty"`
		ARM int `json:"ARM,omitempty"`
		AST int `json:"AST,omitempty"`
		BSM int `json:"BSM,omitempty"`
		CUL int `json:"CUL,omitempty"`
		SCH int `json:"SCH,omitempty"`
		SMN int `json:"SMN,omitempty"`
		WHM int `json:"WHM,omitempty"`
		RDM int `json:"RDM,omitempty"`
		ACN int `json:"ACN,omitempty"`
		DRG int `json:"DRG,omitempty"`
		FSH int `json:"FSH,omitempty"`
		NIN int `json:"NIN,omitempty"`
		PGL int `json:"PGL,omitempty"`
		LTW int `json:"LTW,omitempty"`
		MCH int `json:"MCH,omitempty"`
		Name string `json:"Name,omitempty"`
		Name_en string `json:"Name_en,omitempty"`
		THM int `json:"THM,omitempty"`
		BRD int `json:"BRD,omitempty"`
		DRK int `json:"DRK,omitempty"`
		MNK int `json:"MNK,omitempty"`
		ALC int `json:"ALC,omitempty"`
		BTN int `json:"BTN,omitempty"`
		MIN int `json:"MIN,omitempty"`
		Name_ja string `json:"Name_ja,omitempty"`
		BLM int `json:"BLM,omitempty"`
		ID int `json:"ID,omitempty"`
		Name_de string `json:"Name_de,omitempty"`
		WAR int `json:"WAR,omitempty"`
	}
	Description_ja string `json:"Description_ja,omitempty"`
	Description_kr interface{} `json:"Description_kr,omitempty"`
	QuestRequirement interface{} `json:"QuestRequirement,omitempty"`
	Description string `json:"Description,omitempty"`
	DescriptionJSON_fr []interface{} `json:"DescriptionJSON_fr,omitempty"`
	DescriptionJSON_kr interface{} `json:"DescriptionJSON_kr,omitempty"`
	QuestRequirementTarget string `json:"QuestRequirementTarget,omitempty"`
	ClassJobCategoryTargetID int `json:"ClassJobCategoryTargetID,omitempty"`
	Description_de string `json:"Description_de,omitempty"`
	GSMTargetID int `json:"GSMTargetID,omitempty"`
	GameContentLinks struct {
		CraftAction struct {
			ALC []interface{} `json:"ALC,omitempty"`
		}
	}
	BSMTargetID int `json:"BSMTargetID,omitempty"`
	CRPTarget string `json:"CRPTarget,omitempty"`
	ALC interface{} `json:"ALC,omitempty"`
	AnimationStart struct {
		ID int `json:"ID,omitempty"`
		Key string `json:"Key,omitempty"`
		Key_en string `json:"Key_en,omitempty"`
	}
	DescriptionJSON_cn interface{} `json:"DescriptionJSON_cn,omitempty"`
	GSMTarget string `json:"GSMTarget,omitempty"`
}
