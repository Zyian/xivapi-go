// Code generated by generate_structs - DO NOT EDIT.

package xivapi

type TutorialHealer struct {
	Objective struct {
		RewardRangedTarget string `json:"RewardRangedTarget,omitempty"`
		RewardTankTarget string `json:"RewardTankTarget,omitempty"`
		RewardMelee interface{} `json:"RewardMelee,omitempty"`
		RewardMeleeTargetID int `json:"RewardMeleeTargetID,omitempty"`
		ObjectiveTargetID int `json:"ObjectiveTargetID,omitempty"`
		RewardMeleeTarget string `json:"RewardMeleeTarget,omitempty"`
		RewardRangedTargetID int `json:"RewardRangedTargetID,omitempty"`
		RewardTank interface{} `json:"RewardTank,omitempty"`
		Gil int `json:"Gil,omitempty"`
		ObjectiveTarget string `json:"ObjectiveTarget,omitempty"`
		Objective struct {
			Text string `json:"Text,omitempty"`
			Text_de string `json:"Text_de,omitempty"`
			Text_en string `json:"Text_en,omitempty"`
			Text_fr string `json:"Text_fr,omitempty"`
			Text_ja string `json:"Text_ja,omitempty"`
			ID int `json:"ID,omitempty"`
		}
		RewardRanged struct {
			ClassJobRepair interface{} `json:"ClassJobRepair,omitempty"`
			ItemSearchCategory interface{} `json:"ItemSearchCategory,omitempty"`
			ItemUICategoryTargetID int `json:"ItemUICategoryTargetID,omitempty"`
			ModelSub string `json:"ModelSub,omitempty"`
			Description_ja string `json:"Description_ja,omitempty"`
			EquipSlotCategoryTargetID int `json:"EquipSlotCategoryTargetID,omitempty"`
			ItemActionTargetID int `json:"ItemActionTargetID,omitempty"`
			ItemRepair interface{} `json:"ItemRepair,omitempty"`
			ItemSeriesTargetID int `json:"ItemSeriesTargetID,omitempty"`
			ClassJobCategoryTargetID int `json:"ClassJobCategoryTargetID,omitempty"`
			IsCollectable int `json:"IsCollectable,omitempty"`
			BaseParamSpecial4Target string `json:"BaseParamSpecial4Target,omitempty"`
			BaseParamValueSpecial0 int `json:"BaseParamValueSpecial0,omitempty"`
			EquipSlotCategoryTarget string `json:"EquipSlotCategoryTarget,omitempty"`
			GrandCompanyTargetID int `json:"GrandCompanyTargetID,omitempty"`
			SalvageTarget string `json:"SalvageTarget,omitempty"`
			BaseParam0 interface{} `json:"BaseParam0,omitempty"`
			BaseParam4 interface{} `json:"BaseParam4,omitempty"`
			BaseParamSpecial1Target string `json:"BaseParamSpecial1Target,omitempty"`
			BaseParamValueSpecial5 int `json:"BaseParamValueSpecial5,omitempty"`
			CanBeHq int `json:"CanBeHq,omitempty"`
			BaseParamValue1 int `json:"BaseParamValue1,omitempty"`
			IconID int `json:"IconID,omitempty"`
			ItemSpecialBonusTargetID int `json:"ItemSpecialBonusTargetID,omitempty"`
			BaseParam5 interface{} `json:"BaseParam5,omitempty"`
			BaseParamSpecial2Target string `json:"BaseParamSpecial2Target,omitempty"`
			ItemActionTarget string `json:"ItemActionTarget,omitempty"`
			StackSize int `json:"StackSize,omitempty"`
			BaseParam5Target string `json:"BaseParam5Target,omitempty"`
			GrandCompany interface{} `json:"GrandCompany,omitempty"`
			GrandCompanyTarget string `json:"GrandCompanyTarget,omitempty"`
			IsGlamourous int `json:"IsGlamourous,omitempty"`
			IsIndisposable int `json:"IsIndisposable,omitempty"`
			ItemSearchCategoryTarget string `json:"ItemSearchCategoryTarget,omitempty"`
			MaterializeType int `json:"MaterializeType,omitempty"`
			BlockRate int `json:"BlockRate,omitempty"`
			DamageMag int `json:"DamageMag,omitempty"`
			IsEquippable int `json:"IsEquippable,omitempty"`
			Name_fr string `json:"Name_fr,omitempty"`
			BaseParam0Target string `json:"BaseParam0Target,omitempty"`
			BaseParamSpecial3TargetID int `json:"BaseParamSpecial3TargetID,omitempty"`
			EquipSlotCategory interface{} `json:"EquipSlotCategory,omitempty"`
			BaseParam2Target string `json:"BaseParam2Target,omitempty"`
			ClassJobRepairTarget string `json:"ClassJobRepairTarget,omitempty"`
			FilterGroup int `json:"FilterGroup,omitempty"`
			Plural string `json:"Plural,omitempty"`
			Singular_en string `json:"Singular_en,omitempty"`
			BaseParamSpecial2 interface{} `json:"BaseParamSpecial2,omitempty"`
			BaseParamSpecial3Target string `json:"BaseParamSpecial3Target,omitempty"`
			BaseParamValue2 int `json:"BaseParamValue2,omitempty"`
			ItemAction interface{} `json:"ItemAction,omitempty"`
			ItemRepairTarget string `json:"ItemRepairTarget,omitempty"`
			LevelEquip int `json:"LevelEquip,omitempty"`
			Description_fr string `json:"Description_fr,omitempty"`
			IsAdvancedMeldingPermitted int `json:"IsAdvancedMeldingPermitted,omitempty"`
			LevelItem int `json:"LevelItem,omitempty"`
			MateriaSlotCount int `json:"MateriaSlotCount,omitempty"`
			BaseParamSpecial4 interface{} `json:"BaseParamSpecial4,omitempty"`
			BaseParamSpecial5TargetID int `json:"BaseParamSpecial5TargetID,omitempty"`
			Description string `json:"Description,omitempty"`
			Name_de string `json:"Name_de,omitempty"`
			Plural_de string `json:"Plural_de,omitempty"`
			PriceMid int `json:"PriceMid,omitempty"`
			BaseParamValueSpecial4 int `json:"BaseParamValueSpecial4,omitempty"`
			Icon string `json:"Icon,omitempty"`
			ClassJobUseTargetID int `json:"ClassJobUseTargetID,omitempty"`
			IsUntradable int `json:"IsUntradable,omitempty"`
			BaseParam0TargetID int `json:"BaseParam0TargetID,omitempty"`
			BaseParam1TargetID int `json:"BaseParam1TargetID,omitempty"`
			BaseParamSpecial4TargetID int `json:"BaseParamSpecial4TargetID,omitempty"`
			BaseParamValue3 int `json:"BaseParamValue3,omitempty"`
			ClassJobUseTarget string `json:"ClassJobUseTarget,omitempty"`
			DefensePhys int `json:"DefensePhys,omitempty"`
			EquipRestriction int `json:"EquipRestriction,omitempty"`
			PriceLow int `json:"PriceLow,omitempty"`
			BaseParamSpecial1TargetID int `json:"BaseParamSpecial1TargetID,omitempty"`
			BaseParamSpecial5Target string `json:"BaseParamSpecial5Target,omitempty"`
			BaseParamValue0 int `json:"BaseParamValue0,omitempty"`
			BaseParamValueSpecial2 int `json:"BaseParamValueSpecial2,omitempty"`
			Singular string `json:"Singular,omitempty"`
			BaseParam3Target string `json:"BaseParam3Target,omitempty"`
			BaseParam4TargetID int `json:"BaseParam4TargetID,omitempty"`
			BaseParamSpecial1 interface{} `json:"BaseParamSpecial1,omitempty"`
			Block int `json:"Block,omitempty"`
			DamagePhys int `json:"DamagePhys,omitempty"`
			StartsWithVowel int `json:"StartsWithVowel,omitempty"`
			BaseParamValueSpecial3 int `json:"BaseParamValueSpecial3,omitempty"`
			Plural_en string `json:"Plural_en,omitempty"`
			Plural_fr string `json:"Plural_fr,omitempty"`
			BaseParamSpecial0Target string `json:"BaseParamSpecial0Target,omitempty"`
			ID int `json:"ID,omitempty"`
			ItemUICategoryTarget string `json:"ItemUICategoryTarget,omitempty"`
			BaseParam1Target string `json:"BaseParam1Target,omitempty"`
			BaseParam2 interface{} `json:"BaseParam2,omitempty"`
			Rarity int `json:"Rarity,omitempty"`
			Salvage interface{} `json:"Salvage,omitempty"`
			BaseParamSpecial2TargetID int `json:"BaseParamSpecial2TargetID,omitempty"`
			Description_de string `json:"Description_de,omitempty"`
			IsDyeable int `json:"IsDyeable,omitempty"`
			ItemSeriesTarget string `json:"ItemSeriesTarget,omitempty"`
			Plural_ja string `json:"Plural_ja,omitempty"`
			Singular_fr string `json:"Singular_fr,omitempty"`
			BaseParam3 interface{} `json:"BaseParam3,omitempty"`
			BaseParam3TargetID int `json:"BaseParam3TargetID,omitempty"`
			BaseParamValue5 int `json:"BaseParamValue5,omitempty"`
			BaseParamValueSpecial1 int `json:"BaseParamValueSpecial1,omitempty"`
			ClassJobRepairTargetID int `json:"ClassJobRepairTargetID,omitempty"`
			DelayMs int `json:"DelayMs,omitempty"`
			Name_en string `json:"Name_en,omitempty"`
			AdditionalData int `json:"AdditionalData,omitempty"`
			BaseParamSpecial5 interface{} `json:"BaseParamSpecial5,omitempty"`
			Description_en string `json:"Description_en,omitempty"`
			IsUnique int `json:"IsUnique,omitempty"`
			ItemGlamour interface{} `json:"ItemGlamour,omitempty"`
			ItemSeries interface{} `json:"ItemSeries,omitempty"`
			ItemSpecialBonus interface{} `json:"ItemSpecialBonus,omitempty"`
			BaseParamSpecial3 interface{} `json:"BaseParamSpecial3,omitempty"`
			CooldownS int `json:"CooldownS,omitempty"`
			ItemGlamourTarget string `json:"ItemGlamourTarget,omitempty"`
			ItemGlamourTargetID int `json:"ItemGlamourTargetID,omitempty"`
			IsCrestWorthy int `json:"IsCrestWorthy,omitempty"`
			ItemSpecialBonusTarget string `json:"ItemSpecialBonusTarget,omitempty"`
			ItemUICategory interface{} `json:"ItemUICategory,omitempty"`
			SalvageTargetID int `json:"SalvageTargetID,omitempty"`
			Singular_ja string `json:"Singular_ja,omitempty"`
			BaseParam4Target string `json:"BaseParam4Target,omitempty"`
			BaseParamModifier int `json:"BaseParamModifier,omitempty"`
			BaseParamSpecial0TargetID int `json:"BaseParamSpecial0TargetID,omitempty"`
			ClassJobCategory interface{} `json:"ClassJobCategory,omitempty"`
			Name_ja string `json:"Name_ja,omitempty"`
			AetherialReduce int `json:"AetherialReduce,omitempty"`
			ClassJobCategoryTarget string `json:"ClassJobCategoryTarget,omitempty"`
			IsPvP int `json:"IsPvP,omitempty"`
			Name string `json:"Name,omitempty"`
			BaseParam1 interface{} `json:"BaseParam1,omitempty"`
			ItemRepairTargetID int `json:"ItemRepairTargetID,omitempty"`
			ItemSearchCategoryTargetID int `json:"ItemSearchCategoryTargetID,omitempty"`
			ItemSpecialBonusParam int `json:"ItemSpecialBonusParam,omitempty"`
			Singular_de string `json:"Singular_de,omitempty"`
			BaseParam2TargetID int `json:"BaseParam2TargetID,omitempty"`
			BaseParamValue4 int `json:"BaseParamValue4,omitempty"`
			ClassJobUse interface{} `json:"ClassJobUse,omitempty"`
			ModelMain string `json:"ModelMain,omitempty"`
			BaseParam5TargetID int `json:"BaseParam5TargetID,omitempty"`
			BaseParamSpecial0 interface{} `json:"BaseParamSpecial0,omitempty"`
			DefenseMag int `json:"DefenseMag,omitempty"`
		}
		RewardTankTargetID int `json:"RewardTankTargetID,omitempty"`
		Exp int `json:"Exp,omitempty"`
		ID int `json:"ID,omitempty"`
	}
	ObjectiveTarget string `json:"ObjectiveTarget,omitempty"`
	ObjectiveTargetID int `json:"ObjectiveTargetID,omitempty"`
	Url string `json:"Url,omitempty"`
	GameContentLinks []interface{} `json:"GameContentLinks,omitempty"`
	ID int `json:"ID,omitempty"`
}
