// Code generated by generate_structs - DO NOT EDIT.

package xivapi

type ScenarioTreeTips struct {
	Url string `json:"Url,omitempty"`
	Tips1Target string `json:"Tips1Target,omitempty"`
	Tips2 struct {
		Image struct {
			ID int `json:"ID,omitempty"`
			Image string `json:"Image,omitempty"`
			ImageID int `json:"ImageID,omitempty"`
		}
		ImageTarget string `json:"ImageTarget,omitempty"`
		ImageTargetID int `json:"ImageTargetID,omitempty"`
		Type interface{} `json:"Type,omitempty"`
		TypeTarget string `json:"TypeTarget,omitempty"`
		TypeTargetID int `json:"TypeTargetID,omitempty"`
		ID int `json:"ID,omitempty"`
	}
	Tips2TargetID int `json:"Tips2TargetID,omitempty"`
	Tips1TargetID int `json:"Tips1TargetID,omitempty"`
	Tips2Target string `json:"Tips2Target,omitempty"`
	GameContentLinks []interface{} `json:"GameContentLinks,omitempty"`
	ID int `json:"ID,omitempty"`
	Tips1 struct {
		ID int `json:"ID,omitempty"`
		Level struct {
			MapTargetID int `json:"MapTargetID,omitempty"`
			Object int `json:"Object,omitempty"`
			Y int `json:"Y,omitempty"`
			Z int `json:"Z,omitempty"`
			ID int `json:"ID,omitempty"`
			Map interface{} `json:"Map,omitempty"`
			Territory interface{} `json:"Territory,omitempty"`
			TerritoryTarget string `json:"TerritoryTarget,omitempty"`
			X int `json:"X,omitempty"`
			MapTarget string `json:"MapTarget,omitempty"`
			Radius int `json:"Radius,omitempty"`
			EventId int `json:"EventId,omitempty"`
			TerritoryTargetID int `json:"TerritoryTargetID,omitempty"`
			Type int `json:"Type,omitempty"`
			Yaw int `json:"Yaw,omitempty"`
		}
		LevelTarget string `json:"LevelTarget,omitempty"`
		LevelTargetID int `json:"LevelTargetID,omitempty"`
	}
}
