// Code generated by generate_structs - DO NOT EDIT.

package xivapi

type RecommendContents struct {
	GameContentLinks []interface{} `json:"GameContentLinks,omitempty"`
	ID int `json:"ID,omitempty"`
	Level struct {
		Territory struct {
			TerritoryIntendedUse int `json:"TerritoryIntendedUse,omitempty"`
			MapTargetID int `json:"MapTargetID,omitempty"`
			PlaceName interface{} `json:"PlaceName,omitempty"`
			PlaceNameRegion interface{} `json:"PlaceNameRegion,omitempty"`
			PlaceNameTarget string `json:"PlaceNameTarget,omitempty"`
			Name string `json:"Name,omitempty"`
			WeatherRate int `json:"WeatherRate,omitempty"`
			Map interface{} `json:"Map,omitempty"`
			PlaceNameRegionTargetID int `json:"PlaceNameRegionTargetID,omitempty"`
			AetheryteTarget string `json:"AetheryteTarget,omitempty"`
			ArrayEventHandler interface{} `json:"ArrayEventHandler,omitempty"`
			Bg_en string `json:"Bg_en,omitempty"`
			ID int `json:"ID,omitempty"`
			Bg string `json:"Bg,omitempty"`
			MapTarget string `json:"MapTarget,omitempty"`
			Name_en string `json:"Name_en,omitempty"`
			PlaceNameRegionTarget string `json:"PlaceNameRegionTarget,omitempty"`
			Aetheryte interface{} `json:"Aetheryte,omitempty"`
			AetheryteTargetID int `json:"AetheryteTargetID,omitempty"`
			ArrayEventHandlerTarget string `json:"ArrayEventHandlerTarget,omitempty"`
			ArrayEventHandlerTargetID int `json:"ArrayEventHandlerTargetID,omitempty"`
			PlaceNameTargetID int `json:"PlaceNameTargetID,omitempty"`
			PlaceNameZone interface{} `json:"PlaceNameZone,omitempty"`
			PlaceNameZoneTarget string `json:"PlaceNameZoneTarget,omitempty"`
			PlaceNameZoneTargetID int `json:"PlaceNameZoneTargetID,omitempty"`
		}
		Y int `json:"Y,omitempty"`
		Yaw int `json:"Yaw,omitempty"`
		ID int `json:"ID,omitempty"`
		Map struct {
			ID int `json:"ID,omitempty"`
			MapFilename string `json:"MapFilename,omitempty"`
			PlaceNameSub interface{} `json:"PlaceNameSub,omitempty"`
			SizeFactor int `json:"SizeFactor,omitempty"`
			TerritoryType interface{} `json:"TerritoryType,omitempty"`
			TerritoryTypeTargetID int `json:"TerritoryTypeTargetID,omitempty"`
			MapMarkerRange int `json:"MapMarkerRange,omitempty"`
			OffsetX int `json:"OffsetX,omitempty"`
			PlaceNameRegion interface{} `json:"PlaceNameRegion,omitempty"`
			PlaceNameRegionTarget string `json:"PlaceNameRegionTarget,omitempty"`
			PlaceNameSubTarget string `json:"PlaceNameSubTarget,omitempty"`
			PlaceNameSubTargetID int `json:"PlaceNameSubTargetID,omitempty"`
			PlaceNameTargetID int `json:"PlaceNameTargetID,omitempty"`
			TerritoryTypeTarget string `json:"TerritoryTypeTarget,omitempty"`
			DiscoveryIndex int `json:"DiscoveryIndex,omitempty"`
			MapFilenameId string `json:"MapFilenameId,omitempty"`
			PlaceName interface{} `json:"PlaceName,omitempty"`
			PlaceNameRegionTargetID int `json:"PlaceNameRegionTargetID,omitempty"`
			PlaceNameTarget string `json:"PlaceNameTarget,omitempty"`
			DiscoveryArrayByte int `json:"DiscoveryArrayByte,omitempty"`
			Hierarchy int `json:"Hierarchy,omitempty"`
			OffsetY int `json:"OffsetY,omitempty"`
		}
		MapTarget string `json:"MapTarget,omitempty"`
		MapTargetID int `json:"MapTargetID,omitempty"`
		Z int `json:"Z,omitempty"`
		Object int `json:"Object,omitempty"`
		Radius int `json:"Radius,omitempty"`
		TerritoryTarget string `json:"TerritoryTarget,omitempty"`
		Type int `json:"Type,omitempty"`
		X int `json:"X,omitempty"`
		EventId int `json:"EventId,omitempty"`
		TerritoryTargetID int `json:"TerritoryTargetID,omitempty"`
	}
	LevelTarget string `json:"LevelTarget,omitempty"`
	LevelTargetID int `json:"LevelTargetID,omitempty"`
	Url string `json:"Url,omitempty"`
}
